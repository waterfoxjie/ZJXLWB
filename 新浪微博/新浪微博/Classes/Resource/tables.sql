-- 创建新浪微博数据表 --
/**
 * statusID（主键）-- 微博代号
   取自新浪微博服务器
 * status -- 放置从服务器中获取的单条微博的JSON字符串
 * userID -- 保存登录用户的ID（区分用户）
 * createTime -- 本地保存数据的时间
 */
CREATE TABLE IF NOT EXISTS "T_Status" (
"statusID" INTEGER NOT NULL,
"status" TEXT,
"userID" INTEGER,
"createTime" TEXT DEFAULT (datetime('now','localtime')),
PRIMARY KEY("statusID")
);