//
//  ComposeViewContr.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/8.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit
import SVProgressHUD

// 微博文字最长的长度
private let KStatusTextMaxLength = 140

class ComposeViewContr: UIViewController , UITextViewDelegate {
    
    // MARK: - 照片选择器
    private lazy var photoChooseVC : PhotoChooseCollContr = PhotoChooseCollContr()
    
    // MARK: - 键盘输入视图
    private lazy var emoticonVC : EmoticonViewContr = EmoticonViewContr { [weak self] (emoticon) -> () in
        
        self?.textView.insertEmoticon(emoticon)
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // 添加键盘通知
        addKeyboardOberserver()
        
    }
    
    // 删除
    deinit {
    
        removeKeyboardOberserver()
    }
    
    // 键盘成为第一响应者
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        // 让textView成为第一响应者
        textView.becomeFirstResponder()
        
    }
    
    
    // MARK: - 导航栏按钮方法
    // 发布微博
    func sendStatus() {
        
        // 做微博文字长短的检测（新浪微博官方是规定140字以内）
        // 获取当前要发送的文本
        let text = textView.emoticonText
        
        // 进行比较
        if text.characters.count > KStatusTextMaxLength {
        
            // 提示用户输入的文本太长
            SVProgressHUD.showErrorWithStatus("输入的文本文字过长~~", maskType: SVProgressHUDMaskType.Gradient)
            
            return
            
        }
    
        // 调用发布微博的方法
        // 如果没有图片，则image == nil
        let image = photoChooseVC.photos.last
        
        NetworkTools.shareNetworkTools.sendStatus(textView.emoticonText, image: image){ (result, error) -> () in
            
            if error != nil {
            
                // 表示有错误
                SVProgressHUD.showErrorWithStatus("您的网络不给力~~", maskType: SVProgressHUDMaskType.Gradient)
                
                return
            }
        }
        
        // 发送微博之后，退回首页
        self.close()
        
    }

    // 取消
    func close() {
    
        // 关闭控制器
        dismissViewControllerAnimated(true, completion: nil)
        
        // 注销第一响应者
        textView.resignFirstResponder()
        
    }
    
    
    // MARK: - toolbar按钮监听
    func inputEmoticon() {
        
        // 关闭键盘
        textView.resignFirstResponder()
        
        // 设置输入键盘
        // 判断当前使用的键盘是否是系统默认的，进行切换
        textView.inputView = (textView.inputView == nil) ? emoticonVC.view : nil
        
        // 激活键盘
        textView.becomeFirstResponder()
        
        
    }
    
    // MARK: - 键盘监听方法
    // 设置toolbar底部约束
    private var toolbarButtomCons : NSLayoutConstraint?
    
    // 添加键盘通知
    private func addKeyboardOberserver() {
    
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardChanged:", name: UIKeyboardWillChangeFrameNotification, object: nil)
    }
    
    
    // 删除键盘通知
    private func removeKeyboardOberserver() {
    
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    // MARK: - 实现textView的代理方法
    func textViewDidChange(textView: UITextView) {
        
        // 设置占位标签 & 按钮
        // hasText() ：判断是否有文字（既能检查到text，也能检查到attributeText）
        labPlaceholder.hidden = textView.hasText()

        navigationItem.rightBarButtonItem?.enabled = textView.hasText()
        
        // 计算用户输入的长度，给予提示
        let text = textView.emoticonText
        
        let len = KStatusTextMaxLength - text.characters.count
        
        // 显示
        lengthTipLabel.text = "\(len)"
        
        // 设置颜色
        lengthTipLabel.textColor = len > 0 ? UIColor.grayColor() : UIColor.redColor()
        
    }
    
    
    // 通知方法
    func keyboardChanged(n : NSNotification) {
        
        // 获取目标frame
        let rect = n.userInfo![UIKeyboardFrameEndUserInfoKey]!.CGRectValue
        
        // 设置动画时间选项
        /// 7 的功能 ：(1)如果连续两个动画，会执行后面的动画，前面的动画直接停止 ；（2）使用此选项的时候，动画的时长就是固定0.5秒，动画时长属性无效
        let curve = n.userInfo!["UIKeyboardAnimationDurationUserInfoKey"]!.integerValue
        
        // 获取动画时长
        let durantion = n.userInfo![UIKeyboardAnimationDurationUserInfoKey]!.doubleValue
        
        // 修改toolbar约束
        // 下滑时toolbar同时下滑，因此toolbar的底部约束应该根据键盘的y值改变
        toolbarButtomCons?.constant = -(UIScreen.mainScreen().bounds.height - rect.origin.y)
        
        // 设置动画
        UIView.animateWithDuration(durantion) {
            
            // 设置动画时间选项（解决toolbar上下动的bug）
            UIView.setAnimationCurve(UIViewAnimationCurve(rawValue: curve)!)
            
            // 刷新约束
            self.view.layoutIfNeeded()
        }
        
    }
    
    // MARK: - 搭建界面
    override func loadView() {
        
        view = UIView()
        
        view.backgroundColor = UIColor.whiteColor()
        
        // 添加视图（按照先后顺序）
        view.addSubview(textView)
        
        view.addSubview(photoChooseVC.view)
        
        view.addSubview(toolbar)
        
        // 添加子控制器，开发中，如果遇到添加子控制器，一定要使用addChildViewController，否则可能会造成响应者链条断裂
        addChildViewController(photoChooseVC)
        
        // 调用创建视图的方法
        prepareNav()
        
        prepareToolbar()
        
        perpareTextView()
        
        preparePhotoView()
        
    }
    
    
    // MARK: - 创建照片视图
    private func preparePhotoView() {
        
        // 自动布局
        var s = UIScreen.mainScreen().bounds.size
        
        s.height *= 0.65
        
        photoChooseVC.view.ff_AlignInner(type: ff_AlignType.BottomLeft, referView: view, size: CGSize(width: s.width, height: s.height))
        
    }
    
    
    // MARK: - 创建中间的textView
    private func perpareTextView() {
        
        textView.addSubview(labPlaceholder)
        
        textView.backgroundColor = UIColor.whiteColor()
        
        // 设置自动布局
        // 如果是单纯地 nav + scrollView 会自动貂整滚动视图的边距，会将顶部的导航栏的高度预留出来，能实现滚动视图穿透的效果
//        // 将textView的位置向下移64，自行调整
//        textView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
        
        textView.ff_AlignInner(type: ff_AlignType.TopLeft, referView: view, size: nil)
        
        textView.ff_AlignVertical(type: ff_AlignType.TopRight, referView: toolbar, size: nil)
        
        // 占位标签
        labPlaceholder.ff_AlignInner(type: ff_AlignType.TopLeft, referView: textView, size: nil, offset: CGPoint(x: 5, y: 8))
        
        /// 在View中添加长度标签
        view.addSubview(lengthTipLabel)
        
        lengthTipLabel.ff_AlignInner(type: ff_AlignType.BottomRight, referView: textView, size: nil, offset: CGPoint(x: -10, y: -10))
        
        // 设置属性
        lengthTipLabel.text = String(KStatusTextMaxLength)
        
        lengthTipLabel.sizeToFit()
        
    }
    
    
    // MARK: - 创建Toolbar
    private func prepareToolbar() {
        
        toolbar.backgroundColor = UIColor(white: 0.8, alpha: 1.0)
        
        // 设置自动布局
        let cons = toolbar.ff_AlignInner(type: ff_AlignType.BottomCenter, referView: view, size: CGSize( width: UIScreen.mainScreen().bounds.width, height: 44))
        
        toolbarButtomCons = toolbar.ff_Constraint(cons, attribute: NSLayoutAttribute.Bottom)
        
        
        // 添加按钮
        let itemSettings = [["imageName": "compose_toolbar_picture"],
            ["imageName": "compose_mentionbutton_background"],
            ["imageName": "compose_trendbutton_background"],
            ["imageName": "compose_emoticonbutton_background" , "action" : "inputEmoticon"],
            ["imageName": "compose_addbutton_background"]]

        var items = [UIBarButtonItem]()
        
        // 遍历数组，创建barButtom
        for dict in itemSettings {
            
            let item = UIBarButtonItem(imageName: dict["imageName"]!, target: self, action: dict["action"])
            
            items.append(item)
            
            // 加弹簧
            items.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil))
            
        }
        
        // 删除最后一个 item
        items.removeLast()
        
        // 设置 toolBar 的 items
        toolbar.items = items

    }
    
    
    // MARK: - 创建导航栏
    private func prepareNav() {
    
        // 添加左右按钮
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "取消", style: UIBarButtonItemStyle.Plain, target: self, action: "close")
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "发送", style: UIBarButtonItemStyle.Plain, target: self, action: "sendStatus")
        
        // 设置发送按钮不可见
        navigationItem.rightBarButtonItem?.enabled = false
        
        // 创建标题栏
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 35))
        
        navigationItem.titleView = titleView
        
        // 创建两个label
        let labTitle = UILabel(color: UIColor(white: 0.3, alpha: 1.0), fontSize: 15)
        
        labTitle.text = "发微博"
        
        let labName = UILabel(color: UIColor(white: 0.6, alpha: 1.0), fontSize: 13)
        
        labName.text = UserAccount.loadAccount()?.name
        
        // 添加控件
        titleView.addSubview(labTitle)
        
        titleView.addSubview(labName)
        
        // 自动布局
        labTitle.ff_AlignInner(type: ff_AlignType.TopCenter, referView: titleView, size: nil, offset: CGPoint(x: 0, y: 0))
        labName.ff_AlignInner(type: ff_AlignType.BottomCenter, referView: titleView, size: nil, offset: CGPoint(x: 0, y: 0))
        
    }
    
    // MARK: - 懒加载控件
    // 中间文本输入View
    private lazy var textView : UITextView = {
    
        let tv = UITextView()
        
        tv.font = UIFont.systemFontOfSize(16)
        
        // 始终允许垂直拖拽
        tv.alwaysBounceVertical = true
        
        // MARK: - 设置拖拽关闭键盘
        tv.keyboardDismissMode = UIScrollViewKeyboardDismissMode.OnDrag
        
        // 设置代理
        tv.delegate = self
        
        return tv
        
    }()
    
    // 占位标签
    private lazy var labPlaceholder : UILabel = {
        
        let lab = UILabel(color: UIColor.lightGrayColor(), fontSize: 16)
        
        lab.text = "分享新鲜事~~"
        
        return lab
        
    }()
    
    // 长度提示标签
    private lazy var lengthTipLabel = UILabel(color: UIColor.grayColor(), fontSize: 12)
    
    // 工具栏
    private lazy var toolbar = UIToolbar()
    
}
