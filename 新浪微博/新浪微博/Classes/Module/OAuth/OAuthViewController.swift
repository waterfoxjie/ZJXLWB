//
//  OAuthViewController.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/7/31.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit
import SVProgressHUD

class OAuthViewController: UIViewController,UIWebViewDelegate {

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        webView.loadRequest(NSURLRequest(URL: NetworkTools.shareNetworkTools.oathuUrl()))

        
    }

    // MARK: - 搭建界面
    private lazy var webView = UIWebView()
    override func loadView() {
        
        view = webView
        
        // 设置代理，监控webView
        webView.delegate = self
        
        // 设置navigation中得相关属性
        title = "新浪微博"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "关闭", style: UIBarButtonItemStyle.Plain, target: self, action: "close")
    }
    
    // 创建一个关闭界面的函数
    func close() {
        
        SVProgressHUD.dismiss()
    
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    // MARK: - UIWebViewDelegate中的方法
    // 如果请求的URL包含回调地址，要判断参数，否否则返回true
    // 请求参数中是否包含code，可以由此获得请求码
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        // 获取url,生成完整的字符串
        let requestUrl = request.URL!.absoluteString
        
        // 检测 
        print(requestUrl)
        
        // 判断是否包含
        // 不包含则返回true
        if !requestUrl.hasPrefix(NetworkTools.shareNetworkTools.redirectUri) {
        
            return true
        }
        
        // 获取？后面的内容并判断是不是有code
        if let query = request.URL?.query where query.hasPrefix("code=") {
            
            // 检测 print(query)
            // 获取code=后面的内容
            // code就是请求码
            let code = query.substringFromIndex(advance(query.startIndex, "code=".characters.count))
            
            // 检测 
            print(code)
            
            // 换取 TOKEN
            loadAccessToken(code)
            
        }
        else {
        
            close()
        }
    
        return false
    }

    
    // MARK: - 设置网络不给力的情况
    private func loadAccessToken(code : String) {
    
        NetworkTools.shareNetworkTools.loadAccessToken(code) { (result, error) -> () in
            
            if result == nil || error != nil {
            
                self.netError()
                
                return
            }
            
            // 字典转模型
            // 1、用token获取信息，创建用户账户模型
            // 2、异步加载用户信息
            // 3、保存用户信息（模型中完成）
            // 创建完用户信息之后进行加载    loadUserInfo（）加载
            UserAccount(dict: result!).loadUserInfo({ (error) -> () in
                
                if error != nil {
                
                    self.netError()
                    
                    return
                }
                
                // 发送切换控制器的通知
                // 用户登录成功之后进入欢迎界面
                NSNotificationCenter.defaultCenter().postNotificationName(ZJRootViewControllerSwitchNotification, object: false)
                
                // 关闭控制器
                self.close()
                
            })
            
        }
    }
    
    // MARK: - 错误处理
    private func netError() {
    
        SVProgressHUD.showErrorWithStatus("您的网络不给力呀~~")
        
        // 关闭
        // 延迟
        let when = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * NSEC_PER_SEC))
        
        // 尾随闭包
        dispatch_after(when, dispatch_get_main_queue()) {
            
            self.close()
        }

    }



    // 显示圈圈
    func webViewDidStartLoad(webView: UIWebView) {
    
        // SVProgressHUD.show()
    }

    func webViewDidFinishLoad(webView: UIWebView) {
    
        // SVProgressHUD.dismiss()
    }
    
    
    
}
