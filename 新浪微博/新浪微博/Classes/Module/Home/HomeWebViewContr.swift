//
//  HomeWebViewContr.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/29.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit
import SVProgressHUD

class HomeWebViewContr: UIViewController , UIWebViewDelegate {
    
    // 定义一个url属性
    var url : NSURL?
    
    override func loadView() {
        
        // 设置显示view为webView
        view = webView
        
        // 设置代理
        webView.delegate = self
        
        title = "网页"
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // 加载url(先判断url是否存在)
        if url != nil {
        
            webView.loadRequest(NSURLRequest(URL: url!))
            
        }
    }
    
    // MARK: - 实现代理方法
    func webViewDidStartLoad(webView: UIWebView) {
        
        SVProgressHUD.show()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        
        SVProgressHUD.dismiss()
    }

    // MARK: - 懒加载控件
    private lazy var webView = UIWebView()
    
}
