//
//  XLRefreshContr.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/7.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

// 下拉刷新的一个偏移量
private let refreshOffset : CGFloat = -60

/// 下拉刷新控件
class XLRefreshContr: UIRefreshControl {
    
    // MARK: - 重写系统结束刷新方法
    override func endRefreshing() {
        
        super.endRefreshing()
        
        refreshView.stopLoading()
        
    }
    
    
    // MARK: - kvo
    // (1)下拉时，y值越来越小 ; (2)向上拉时，y值变大 ; (3)从初始值向下拉，y值为负值
    // 拉到一定的程序时，会进入自动刷新的状态
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        // 判断如果y值大于0，则直接返回
        if frame.origin.y > 0 {
        
            return
        }
        
        // 判断是否开始刷新,如果进如刷新状态，播放加载动画
        if refreshing {
        
            refreshView.startLoading()
        }
        
        // 如果y小于-偏移量，则进行翻转，否则转回去
        if frame.origin.y < refreshOffset && !refreshView.rotateFlag  {
            
            print("翻过来")
            
            refreshView.rotateFlag = true
           
        }
        
        else if frame.origin.y > refreshOffset && refreshView.rotateFlag {
        
            print("转过去")
            
            refreshView.rotateFlag = false
        }
        
    }
    
    override init() {
        
        super.init()
        
        setUpUI()
        
    }

    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - 撤销kvo监听
    deinit {
    
        removeObserver(self, forKeyPath: "frame")
    }
    
    // MARK: - 搭建界面
    private func setUpUI() {
        
        // kvo监听frame的变化
        self.addObserver(self, forKeyPath: "frame", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
        
        // 隐藏转轮
        tintColor = UIColor.clearColor()
        
        // 添加控件
        addSubview(refreshView)
        
        // 设置自动布局 -- 从xib加载的视图，可以直接取到真实地大小
        refreshView.ff_AlignInner(type: ff_AlignType.CenterCenter, referView: self, size: refreshView.bounds.size)
        
    }
    
    // MARK: - 懒加载控件
    private lazy var refreshView : RefreshView = RefreshView.refreshView()
    
}


/// 下拉刷新视图
class RefreshView : UIView {
    
    // 定义一个旋转标记
    private var rotateFlag = false {
    
        didSet {
        
            rotateTipIcon()
        }
    }
    
    @IBOutlet weak var tipIcon: UIImageView!
    @IBOutlet weak var tipVIew: UIView!
    @IBOutlet weak var loadingIcon: UIImageView!
    
    
    // MARK: - 从xib加载刷新视图
    class func refreshView() -> RefreshView {
    
        return NSBundle.mainBundle().loadNibNamed("RefreshView", owner: nil, options: nil).last as! RefreshView
        
    }
    
    // MARK: - 图片旋转
    private func rotateTipIcon() {
        
        let angle = rotateFlag ? CGFloat(M_PI - 0.01) : CGFloat(-M_PI + 0.01)
    
        // 设置动画，修改图标的frame值
        UIView.animateWithDuration(0.25) { () -> Void in
            
            self.tipIcon.transform = CGAffineTransformRotate(self.tipIcon.transform, angle)
        }
    }
    
    // MARK: - 开始加载动画
    private func startLoading() {
        
        // 判断动画是否已经被添加，若已被添加，则直接return
        // animationForKey 可以拿到添加到图层上得动画
        if loadingIcon.layer.animationForKey("loadingAnim") != nil {
        
            return
            
        }
        
        // 隐藏提示视图
        tipVIew.hidden = true
        
        // 定义动画
        let anim = CABasicAnimation(keyPath: "transform.rotation")
    
        anim.toValue = 2 * M_PI
        
        anim.repeatCount = MAXFLOAT
        
        anim.duration = 0.5
        
        // 添加动画
        loadingIcon.layer.addAnimation(anim, forKey: "loadingAnim")
        
    }
    
    
    // MARK: - 停止加载动画
    private func stopLoading() {
        
        tipVIew.hidden = false
        
        loadingIcon.layer.removeAllAnimations()
    }

    
}