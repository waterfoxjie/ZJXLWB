//
//  PhotoBrowserViewContr.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/19.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

private let PhotoBrowserViewCell = "PhotoBrowserViewCell"

class PhotoBrowserViewContr: UIViewController {
    
    // MARK: - 属性
    // 图片URL数组
    var urls : [NSURL]
    
    // 用户选择的cell的索引
    var index : Int
    
    // 定义一个视图缩放比例
    var photoScale : CGFloat = 1.0

    // 构造函数
    init(urls : [NSURL] , index : Int) {
    
        self.urls = urls
        
        self.index = index
        
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()

    }
    
    // MARK: - 跳转到用户选择的图片
    /// 注意：所有成熟的第三方框架中，只有SWebImage支持gif的播放，但是内存消耗非常大
    // 这里不要写在DidAppear中，写在那里时，视图出现时会首先显示第一张图片，这样会造成图片的跳跃
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        // 获取用户选择的是第几个cell
        let indexpath = NSIndexPath(forItem: index, inSection: 0)
        
        collectionView.scrollToItemAtIndexPath(indexpath, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: false)
    }
    
    // MARK: - 创建界面
    override func loadView() {
        
        // 将视图控制器的大小设大
        var screenBounds = UIScreen.mainScreen().bounds
        
        screenBounds.size.width += 20
        
        /// 设置视图大小，这里非常重要
        view = UIView(frame: screenBounds)
        
        // 搭建界面
        setUpUI()
        
    }
    
    // MARK: - 按钮监听方法
    // 关闭按钮
    func btnCloseChick() {
    
        // 关闭控制器
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    // 保存按钮
    func btnSaveChick() {
    
        // 获取当前显示的索引
        let indexPath = collectionView.indexPathsForVisibleItems().last!
        
        // 获取当前cell
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! PhotoBrowserCell
        
        // 判断图像视图中是否存在图像，因为不一定能下载到图像
        guard let image = cell.imgView.image else {
        
            return
        }
        
        // 保存图像
        // 调用时 ：函数名:外部函数名:
        UIImageWriteToSavedPhotosAlbum(image, self, "image:didFinishSavingWithError:contextInfo:", nil)

    }
    
    // (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo; -- OC函数
    // didFinishSavingWithError 是外部函数名
    func image(image : UIImage , didFinishSavingWithError error : NSError? , contextInfo : AnyObject) {
    
        print(error)
        
        print(image)
    }
    
    
    // MARK: - 搭建界面
    private func setUpUI() {
        
        // 添加控件
        view.addSubview(collectionView)
        
        view.addSubview(btnSave)
        
        view.addSubview(btnClose)
        
        // 设置控件位置
        collectionView.frame = view.bounds
        
        let margin : CGFloat = 15
        
        let btnH : CGFloat = 32
        
        let btnW : CGFloat = 100
        
        let btnY : CGFloat = UIScreen.mainScreen().bounds.height - btnH - margin
        
        let btnSaveX : CGFloat = UIScreen.mainScreen().bounds.width - btnW - margin
        
        btnClose.frame = CGRect(x: margin, y: btnY , width: btnW, height: btnH)
        
        btnSave.frame = CGRect(x: btnSaveX, y: btnY, width: btnW, height: btnH)
        
        // 添加监听方法
        btnClose.addTarget(self, action: "btnCloseChick", forControlEvents: UIControlEvents.TouchUpInside)
        
        btnSave.addTarget(self, action: "btnSaveChick", forControlEvents: UIControlEvents.TouchUpInside)
        
        // 设置独立控件属性（定义cell）
        prepareCollectionView()
        
        
    }
    
    // MARK: - 独立控件
    private func prepareCollectionView() {
    
        // 注册可重用cell
        collectionView.registerClass(PhotoBrowserCell.self, forCellWithReuseIdentifier: PhotoBrowserViewCell)
        
        // 设置数据源
        collectionView.dataSource = self
        
        // 设置布局
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        layout.itemSize = view.bounds.size
        
        layout.minimumInteritemSpacing = 0
        
        layout.minimumLineSpacing = 0
        
        // 水平滚动
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        
        // 设置分页
        collectionView.pagingEnabled = true
        
        collectionView.showsHorizontalScrollIndicator = false
        
        // 设置滚动不会超过边界（即是在第一页以及最后一页时拖动不会出现边界不会出现黑色的部分）
        collectionView.bounces = false
        
    }
    
    // MARK: - 懒加载控件
    // collectionView
    private lazy var collectionView : UICollectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: UICollectionViewFlowLayout())
    
    // 保存按钮
    private lazy var btnSave : UIButton = UIButton(title: "保    存")
    
    // 关闭按钮
    private lazy var btnClose : UIButton = UIButton(title: "关   闭")
    
}

// MARK: - 数据源方法
extension PhotoBrowserViewContr : UICollectionViewDataSource , PhotoBrowserCellDelegate {

    // 返回多少行
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return urls.count
    }
    
    // 每一行有什么
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(PhotoBrowserViewCell, forIndexPath: indexPath) as! PhotoBrowserCell
        
        // 设置图像
        cell.imgUrl = urls[indexPath.item]
        
        cell.photoDelegate = self
        
        return cell
    }
    
    // MARK: - 开始缩放
    func photoBrowserCellZoom(scale: CGFloat) {
        
        // 记录缩放比例
        photoScale = scale
        
        // 设置隐藏控件
        hiddenControls(scale < 1)
        
        // 交互式转场（要遵守协议）
        // 开始转场，判断缩放比例
        if scale < 1.0 {
        
            startInteractiveTransition(self)
            
        } else {
        
            view.transform = CGAffineTransformIdentity
            
            view.alpha = 1
        }
    }
    
    // MARK: - 结束缩放
    func photoBrowserCellEndZoom() {
        
        // 判断缩放比例
        if photoScale < 0.8 {

            completeTransition(true)
            
        } else {
        
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                
                // 恢复显示
                self.view.transform = CGAffineTransformIdentity
                
                self.view.alpha = 1.0
                
                }, completion: { (_) -> Void in
                    
                    self.photoScale = 1
                    
                    self.hiddenControls(false)
            })
            
        }
    }
    
    // MARK: - 获取当前显示图片的索引
    func currenImgIndex() -> NSIndexPath {
    
        return collectionView.indexPathsForVisibleItems().last!
    }
    
    // MARK: - 获取当前显示的图片视图
    func currenImgView() -> UIImageView {
    
        let cell = collectionView.cellForItemAtIndexPath(currenImgIndex()) as! PhotoBrowserCell
        
        return cell.imgView
    }
    
    
    // MARK: - 隐藏控件
    func hiddenControls(hidden : Bool) {
    
        collectionView.backgroundColor = hidden ? UIColor.clearColor() : UIColor.blackColor()
        
        btnClose.hidden = hidden
        
        btnSave.hidden = hidden
        
    }
    
}

extension PhotoBrowserViewContr : UIViewControllerInteractiveTransitioning {

    // 开始交互转场
    func startInteractiveTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        // 进行视图大小的形变
        view.transform = CGAffineTransformMakeScale(photoScale, photoScale)
        
        // 透明度
        view.alpha = photoScale
    }
}

// context ：提供了转场动画所需的所有细节
extension PhotoBrowserViewContr : UIViewControllerContextTransitioning {
    
    // 这个方法是最重要的，结束转场动画
    func completeTransition(didComplete: Bool) {

        // 关闭当前的控制器
        dismissViewControllerAnimated(true, completion: nil)
        
    }

    // 返回一个容器视图
    func containerView() -> UIView? { return view.superview }

    func isAnimated() -> Bool { return true }
    
    func isInteractive() -> Bool { return true }
    
    func transitionWasCancelled() -> Bool { return true }
    
    func presentationStyle() -> UIModalPresentationStyle { return UIModalPresentationStyle.Custom }
    
    func updateInteractiveTransition(percentComplete: CGFloat) {}
    func finishInteractiveTransition() {}
    func cancelInteractiveTransition() {}
    
    func viewControllerForKey(key: String) -> UIViewController? { return self }
    
    func viewForKey(key: String) -> UIView? { return view }
    
    func targetTransform() -> CGAffineTransform { return CGAffineTransformIdentity }
    
    func initialFrameForViewController(vc: UIViewController) -> CGRect { return CGRectZero }

    func finalFrameForViewController(vc: UIViewController) -> CGRect { return CGRectZero }
    
}



