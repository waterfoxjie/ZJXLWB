//
//  PhotoBrowserCell.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/19.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit
import SDWebImage

// 定义协议
protocol PhotoBrowserCellDelegate : NSObjectProtocol {

    // 图片缩放比例
    func photoBrowserCellZoom(scale: CGFloat)
    
    // 结束缩放
    func photoBrowserCellEndZoom()
    
}

class PhotoBrowserCell: UICollectionViewCell {
    
    // MARK: - 图像url
    var imgUrl : NSURL? {
    
        didSet {
            
            // 显示菊花
            indicator.startAnimating()
            
            // 重新设置scrollView（结局cell重用的bug）
            resetScrollView()
            
            // 清空图片（处理cell被重用的bug）
            imgView.image = nil
            
            // 模拟延迟
            // NSEC_PER_SEC 纳秒  不需要延迟时，将这里设置为0
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0)), dispatch_get_main_queue(), { () -> Void in
                
                // 利用SDWebImage获取网络图像，由于没有设置imgView的大小，所以这里调用时，要设置大小
                self.imgView.sd_setImageWithURL(self.imgUrl) { (image, error, _, _) -> Void in
                    
                    // 隐藏菊花
                    self.indicator.stopAnimating()
                    
                    // 使用SDWebImage获取图像是不一定会获取到
                    // 原因 ：url错误；超时时长（15s），因此这里需要进行判断
                    if image == nil {
                        
                        // 这里需要设置占位图像，对用户进行提醒
                        self.imgView.image = UIImage(named: "imageNill")
                        
                        print("很抱歉，没有获取到您对应的图片...")
                        
                        return
                    }
                    
                    self.setUpImagePosition()
                    
                }
                
            })
            
        }
    }
    
    // MARK: - 重新设置scrollView（结局cell重用的bug）
    private func resetScrollView() {
        
        // 复位imgView的形变(解决第一张图片放大之后后面的图不能放大的bug)
        imgView.transform = CGAffineTransformIdentity
    
        scrollView.contentInset = UIEdgeInsetsZero
        
        scrollView.contentOffset = CGPointZero
        
        scrollView.contentSize = CGSizeZero
        
    }
    
    // MARK: - 图片显示的位置
    private func setUpImagePosition() {
    
        // 设置imgView的大小
        let s = self.displaySize(self.imgView.image!)
        
        // 判断图像的高度是否大于屏幕的高度：是（长图），不是（短图）
        if s.height < scrollView.bounds.height {
        
            // 短图，则将图片显示在正中间
            let y = (scrollView.bounds.height - s.height) * 0.5
            
            // 如果直接在这里修改frame值，会将y的值卡死，导致图片缩放之后无法显示完全
            imgView.frame = CGRect(origin: CGPointZero, size: s)
            
            // 设置间距，能够保证缩放完成后，同样能够显示完整画面
            scrollView.contentInset = UIEdgeInsets(top: y, left: 0, bottom: y, right: 0)
        
        } else {
        
            // 长图，设置可以滑动
            imgView.frame = CGRect(origin: CGPointZero, size: s)
            
            scrollView.contentSize = s
            
        }
        
    }
    
    // MARK: - 计算显示的图片尺寸
    // 图片的宽度与scrollView一致，根据这个宽度进行等比例的缩放，再将图片的位置放于屏幕的中间
    private func displaySize(img : UIImage) -> CGSize {
    
        // 计算图片比例
        let scale = img.size.height / img.size.width
        
        let height = scrollView.bounds.width * scale
        
        return CGSize(width: scrollView.bounds.width, height: height)
        
    }
    
    
    // MARK: - 构造函数
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        setUpUI()
        
    }
      
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
  
    }
    
    // MARK: - 准备界面
    private func setUpUI() {
        
        // 添加控件
        contentView.addSubview(scrollView)
        
        scrollView.addSubview(imgView)
        
        // 菊花添加在contentView上
        contentView.addSubview(indicator)
        
        // 设置控件位置
        scrollView.frame = UIScreen.mainScreen().bounds
        
        indicator.center = scrollView.center
        
        // 设置控件
        perparScrollView()
        
    }
    
    // MARK: - 设置scrollView
    private func perparScrollView() {
    
        // 设置代理
        scrollView.delegate = self
        
        // 设置最大、最小的缩放比例
        scrollView.minimumZoomScale = 0.5
        
        scrollView.maximumZoomScale = 2
        
        
        
    }
    
    
    // MARK: - 懒加载控件
    private lazy var scrollView : UIScrollView = UIScrollView()
    
    lazy var imgView : UIImageView = UIImageView()
    
    // 大菊花
    private lazy var indicator : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
    
    // 设置代理
    weak var photoDelegate : PhotoBrowserCellDelegate?
    
}

// MARK: - sscrollView的代理方法
extension PhotoBrowserCell : UIScrollViewDelegate {

    // 缩放代理方法（View开头）
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        
        return imgView
    }
    
    // 缩放完成之后，就会调用一次这个方法
    // view ：被缩放的view   scale ：缩放完成之后的缩放比例
    func scrollViewDidEndZooming(scrollView: UIScrollView, withView view: UIView?, atScale scale: CGFloat) {
        
        print(scale)
        // 设置大于0.8的之后进行复位，小于时直接解除转场（解决缩放之后图片会回到中点的bug）
        if scale > 0.8 {
        
            // 重新计算间距，使得缩放后的图片仍旧在屏幕中间
            // 这里注意，通过transform改变view的缩放时，view的bounds是不会发生改变的，改变的是frame
            var offsetX = (scrollView.bounds.width - view!.frame.width) * 0.5
 
            var offsetY = (scrollView.bounds.height - view!.frame.height) * 0.5
            
            // 这里需要添加判断，判断边距是否越界，如果越界设置为0
            offsetX = offsetX < 0 ? 0 : offsetX
            
            offsetY = offsetY < 0 ? 0 : offsetY
            
            scrollView.contentInset = UIEdgeInsets(top: offsetY, left: offsetX, bottom: 0, right: 0)
            
        }
        
        // 通知代理结束缩放
        photoDelegate?.photoBrowserCellEndZoom()
    }
    
    // 只要缩放了就会调用的方法
    // transform 参数：a、d ：缩放比例   tx、ty ：位移   a、b、c、d ：共同决定了旋转
    func scrollViewDidZoom(scrollView: UIScrollView) {
        
        photoDelegate?.photoBrowserCellZoom(imgView.transform.a)
        
    }

    
    
}



