//
//  StatusNormalCell.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/6.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

class StatusNormalCell: StatusCell {

    override func setUpUI() {
        
        super.setUpUI()
        
        /// 图片View
        let cons = pictureView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: contentLabView, size: CGSize(width: 290, height: 290), offset: CGPoint(x: 0 , y: statusCellContrMargin))
        
        pictWidthCons = pictureView.ff_Constraint(cons, attribute: NSLayoutAttribute.Width)
        
        pictHeightCons = pictureView.ff_Constraint(cons, attribute: NSLayoutAttribute.Height)
        
        pictTopCons = pictureView.ff_Constraint(cons, attribute: NSLayoutAttribute.Top)

        
    }

}
