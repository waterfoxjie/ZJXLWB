//
//  StatusForwardCell.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/6.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

class StatusForwardCell: StatusCell {
    
    // MARK: - 创建一个模型属性（重写属性）
    /// 重写属性：(1) override添加这一个关键字  (2)子类必须保持与父类相同的行为  (3)不会覆盖父类中的内容
    override var status : XLStatuses? {
    
        didSet {
        
            // 原创微博作者的名称
            let name = status?.retweeted_status?.user?.name ?? ""
            
            // 转发微博的内容
            let text = status?.retweeted_status?.text ?? ""
            
            // 拼接内容
            labForward.text = "@" + name + " : " + text
            
        }
    }
    
    // MARK: - 设置界面
    override func setUpUI() {
        
        /// 重写父类方法，这里一定要写
        super.setUpUI()
        
        /// 添加控件
        contentView.insertSubview(btnBack, belowSubview: pictureView)
        
        contentView.insertSubview(labForward, aboveSubview: pictureView)
        
        /// 自动布局
        // 背景按钮
        btnBack.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: contentLabView, size: nil, offset: CGPoint(x: -statusCellContrMargin * 0.5 * 3, y: statusCellContrMargin))
        
        btnBack.ff_AlignVertical(type: ff_AlignType.TopRight, referView: buttomView, size: nil, offset: CGPoint(x: 0, y: 0))
        
        // 转发标签
        labForward.ff_AlignInner(type: ff_AlignType.TopLeft, referView: btnBack, size: nil, offset: CGPoint(x: statusCellContrMargin * 0.5 * 3, y: statusCellContrMargin))
        
        // 图片
        let cons = pictureView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: labForward, size: CGSize(width: 290, height: 290), offset: CGPoint(x: 0 , y: statusCellContrMargin))
        
        pictWidthCons = pictureView.ff_Constraint(cons, attribute: NSLayoutAttribute.Width)
        
        pictHeightCons = pictureView.ff_Constraint(cons, attribute: NSLayoutAttribute.Height)
        
        pictTopCons = pictureView.ff_Constraint(cons, attribute: NSLayoutAttribute.Top)
        
    }
    

    // MARK: - 懒加载控件
    // 转发标签
    private lazy var labForward : FFLabel = {
        
        let lab = FFLabel(color: UIColor(white: 0.5, alpha: 1.0), fontSize: 14)
        
        // 设置代理（实现链接跳转）
        // 没有实现代理方法时，会自动调用父类的方法
        lab.labelDelegate = self
        
        // 设置换行以及宽度
        lab.numberOfLines = 0
        
        lab.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 3 *  statusCellContrMargin
        
        return lab
        
    }()
    
    // 背景按钮
    private lazy var btnBack : UIButton = {
    
        // 设置背景颜色
        let btn = UIButton()
        
        btn.backgroundColor = UIColor(white: 0.9, alpha: 1.0)
        
        return btn
        
    }()
    
}
