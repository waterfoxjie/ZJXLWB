//
//  StatusPictureView.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/5.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit
import SDWebImage

// 可重用cell标示符
private let StatusPictCellID = "StatusPictCellID"

// 选中微博单元格都图片通知
let StatusPictCellNotification = "StatusPictCellNotification"

// URLKey
let StatusPictCellURLKey = "StatusPictCellURLKey"

// indexPathKey
let StatusPictCellindexPathKey = "StatusPictCellURLKeyindexPathKey"

// MARK: - 微博图片类
class StatusPictureView: UICollectionView {
    
    // MARK: - 定义一个模型数据
    var status : XLStatuses? {
    
        didSet {
        
            // 调用这个方法时，会调用sizeThatFits这个方法
            sizeToFit()
            
            reloadData()
        }
    }
    
    // 在此调用真实计算的结果
    override func sizeThatFits(size: CGSize) -> CGSize {
        
        return calcViewSize()
    }
    
    // MARK: -计算视图大小函数
    private func calcViewSize() -> CGSize {
    
        /// 准备常量
        // 单张图片的大小、间距、每一行最多显示的图片的数量
        let itemSize = CGSize(width: 90, height: 90)
        
        let margin : CGFloat = 10
        
        let rowCount = 3
        
        statusPictVLayout.itemSize = itemSize
        
        /// 根据图片数量计算视图的大小（分4种情况）
        let count = status?.imgUrl?.count ?? 0
        
        // 第一种：无图的情况
        if count == 0 {
        
            return CGSizeZero
        }
        
        // 第二种：一张图片
        if count == 1 {
        
            // 从SDWebImage处获取缓存图片的大小
            // key 就是 URL 的完整字符串
            let key = status?.imgUrl![0].absoluteString
            
            let image = SDWebImageManager.sharedManager().imageCache.imageFromDiskCacheForKey(key)
            
            // 如果没有在SDWebImage中获取到图片的情况出现，则需要判断图片是否存在
            // 图片为空则设置默认图片大小
            var size = CGSize(width: 150, height: 150)
            
            // 不为空的情况，判断图片宽度，防止过宽或者过窄的情况
            if image != nil {
            
                size = image.size
                
                size.width = size.width < 40 ? 40 : size.width
                
                size.width = size.width > UIScreen.mainScreen().bounds.width ? 200 : size.width
                
            }
            
            statusPictVLayout.itemSize = size
            
            return size
        }
        
        // 第三种：4张图片
        if count == 4 {
        
            // 计算宽度、高度
            let w = itemSize.width * 2 + margin
            
            return CGSize(width: w, height: w)
        }
        
        // 最后一种
        // 计算行数
        let row = (count - 1) / rowCount + 1
        
        // 计算高度、宽度
        let w = itemSize.width * CGFloat(rowCount) + margin * CGFloat(rowCount - 1)
        
        let h = itemSize.height * CGFloat(row) + margin * CGFloat(row - 1)
        
        return CGSize(width: w, height: h)
        
    }
    
    // 定义一个UICollectionViewFlowLayout的常量
    let statusPictVLayout = UICollectionViewFlowLayout()
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        
        super.init(frame: frame, collectionViewLayout: statusPictVLayout)
        
        backgroundColor = UIColor.lightGrayColor()
        
        // 注册可重用cell
        registerClass(StatusPictCell.self, forCellWithReuseIdentifier: StatusPictCellID)
        
        // 设置数据源为自己
        self.dataSource = self
        
        // 设置代理
        self.delegate = self
        
    }

    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - 实现数据源方法、代理方法
extension StatusPictureView : UICollectionViewDataSource , UICollectionViewDelegate {
    
    // 实现代理方法
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
//        // 测试代理
//        let v = UIView()
//        v.backgroundColor = UIColor.redColor()
//        v.frame = self.cellFullScreenFrame(indexPath)
//        UIApplication.sharedApplication().keyWindow?.addSubview(v)
        
        // 通知中需要传递的内容 ：大图URL的数组，用户选择的单元格的indexPath
        /**
        aName: 通知的名字，向通知中心发送，由通知中心向所有的监听者`广播`
        object: 发生事件的时候，传递一个对象，可以是 self
        userInfo: 字典，可以随着通知发送更多的消息
        这里需要传两个键值对，一个是URL，一个是indexPath
        
        通知应用场景:
        1. 如果视图层次嵌套很深，通过代理逐层传递非常繁琐，可以使用通知
        2. 使用通知可以让对象之间解耦
        
        注意事项：        
        1. 通知是没有返回值的
        2. 通知中心的方法是同步执行的！先执行监听方法，执行完毕后，才会执行后续的代码
        */
        NSNotificationCenter.defaultCenter().postNotificationName(StatusPictCellNotification, object: self,
            userInfo: [StatusPictCellURLKey : status!.largeImgUrl! , StatusPictCellindexPathKey : indexPath])

    }

    // 返回多少行数据
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return status?.imgUrl?.count ?? 0
    }
    
    // 每个cell中有什么
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = dequeueReusableCellWithReuseIdentifier(StatusPictCellID, forIndexPath: indexPath) as! StatusPictCell
        
        cell.imgURl = status!.imgUrl![indexPath.item]
        
        return cell
    }
    
    // MARK: - 计算cell在主窗口中的位置
    func cellScreenFrame(indexPath : NSIndexPath) -> CGRect {
    
        // 获取cell
        let cell = cellForItemAtIndexPath(indexPath)!
        
        // 计算cell在主窗口中的位置
        let rect = convertRect(cell.frame, toCoordinateSpace: UIApplication.sharedApplication().keyWindow!)
        
        return rect
        
    }
    
    // MARK: - 计算cell放大之后在主窗口的位置
    func cellFullScreenFrame(indexPath : NSIndexPath) -> CGRect {
    
        // 图片放大之后的大小与缩略的宽高比例是一样的
        // 获取图片的缩略图
        let imgKey = status?.imgUrl![indexPath.item].absoluteString
        
        let img = SDWebImageManager.sharedManager().imageCache.imageFromDiskCacheForKey(imgKey)
        
        let screenRect = UIScreen.mainScreen().bounds
        
        var bigImgRect : CGRect = CGRectZero
        
        // 判断图片是否存在
        if img != nil {
        
            // 根据图片的宽高比例计算大图的尺寸
            let scale = img.size.height / img.size.width
            
            let bigImgH = screenRect.width * scale
            
            // 判断长短图
            var y : CGFloat = 0
            
            // 短图的情况
            if bigImgH < screenRect.height {
            
                y = (screenRect.height - bigImgH) * 0.5
            }
            
            bigImgRect = CGRect(x: 0, y: y, width: screenRect.width, height: bigImgH)
            
        }
        
        return bigImgRect
    }
    
}

// MARK: - 自定义cell
class StatusPictCell : UICollectionViewCell {
    
    // 图片url
    var imgURl : NSURL? {
    
        didSet {
        
            imgPictView.sd_setImageWithURL(imgURl)
            
            // 根据url的扩展名判断是否显示gif图标
            /// Xcode 7.0 bate 5 之后将url转为String时都必须加 as NSString 这个
            // 获取图片的扩展名，并将大写的GIF转换成小写
            let pathExtension = (imgURl!.absoluteString as NSString).pathExtension.lowercaseString == "gif"
            
            // 不是gif则不显示
            gifImgView.hidden = !pathExtension
        }
    }
    
    // 重写构造函数
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        setUpUI()
    
    }

    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    // 设置视图
    private func setUpUI() {
    
        // 添加控件
        addSubview(imgPictView)
        
        imgPictView.addSubview(gifImgView)
        
        // 自动布局
        imgPictView.ff_Fill(contentView)
        
        gifImgView.ff_AlignInner(type: ff_AlignType.BottomRight, referView: imgPictView, size: nil, offset: CGPoint(x: 0, y: 0))
        
    }

    // 懒加载控件
    // 图片空间
    private lazy var imgPictView : UIImageView = {
        
        let pict = UIImageView()
        
        // 设置图片填充模式
        pict.contentMode = UIViewContentMode.ScaleAspectFill
        
        // 设置超出边界的裁掉
        pict.clipsToBounds = true
        
        return pict
    
    }()
    
    // 显示gif图片空间
    private lazy var gifImgView : UIImageView = UIImageView(image: UIImage(named: "timeline_image_gif"))
    
}




