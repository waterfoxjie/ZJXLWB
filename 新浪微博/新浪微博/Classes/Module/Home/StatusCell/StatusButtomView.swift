//
//  StatusButtomView.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/5.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

class StatusButtomView: UIView {
    

    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        backgroundColor = UIColor.redColor()
        
        setUpUI()
        
    }

    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - 设置视图
    private func setUpUI() {
        
        // 设置背景颜色
        backgroundColor = UIColor(white: 0.97, alpha: 1.0)
        
        // 添加控件
        addSubview(forwardBtn)
        
        addSubview(commentBtn)
        
        addSubview(likeBtn)
    
        // 自动布局(三个控件水平平铺)
        ff_HorizontalTile([forwardBtn , commentBtn , likeBtn], insets: UIEdgeInsetsZero)
        
    }
    
    // MARK: - 懒加载
    private lazy var forwardBtn : UIButton = UIButton(title: " 转发", imgName: "timeline_icon_retweet")
    
    private lazy var commentBtn : UIButton = UIButton(title: " 评论", imgName: "timeline_icon_comment")
    
    private lazy var likeBtn : UIButton = UIButton(title: " 赞", imgName: "timeline_icon_unlike")
    
    

}
