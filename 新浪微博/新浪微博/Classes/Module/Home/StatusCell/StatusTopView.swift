//
//  StatusTopView.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/4.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit
import SDWebImage

class StatusTopView: UIView {
    
    // MARK: - 创建一个模型属性
    var status : XLStatuses? {
    
        didSet {
            
            if let url = status?.user?.imgUrl {
            
                imgIcon.sd_setImageWithURL(url)
            }
        
            labName.text = status?.user?.name ?? ""
            
            imgMemberIcon.image = status?.user?.imgMbrank
            
            imgVipIcon.image = status?.user?.imgVer
            
            // 时间是不断改变的
            labTime.text = NSDate.sinaDate(status?.created_at ?? "")?.dateDesctiption
            
            // 应用只需获取一次即可
            labSource.text = "来自 " + "\(status?.source)"
            
        }
    }
    

    // MARK: - 重写构造方法
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        // 设置背景色
        backgroundColor = UIColor.clearColor()
        setUpUI()
    }

    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - 设置界面
    private func setUpUI() {
        
        // 添加控件
        addSubview(imgIcon)
        
        addSubview(labName)
        
        addSubview(labTime)
        
        addSubview(labSource)
        
        addSubview(imgMemberIcon)
        
        addSubview(imgVipIcon)
        
        // 设置自动布局
        // 头像
        imgIcon.ff_AlignInner(type: ff_AlignType.TopLeft, referView: self, size: CGSize(width: 40, height: 40), offset: CGPoint(x: 15, y: 11))
        
        // 名字
        labName.ff_AlignHorizontal(type: ff_AlignType.TopRight, referView:imgIcon , size: nil, offset: CGPoint(x: 15, y: 2))
        
        // 时间
        labTime.ff_AlignHorizontal(type: ff_AlignType.BottomRight, referView: imgIcon, size: nil, offset: CGPoint(x: 15, y: -2))
        
        // 来源
        labSource.ff_AlignHorizontal(type: ff_AlignType.BottomRight, referView: labTime, size: nil, offset: CGPoint(x: 10, y: 0))
        
        // 会员
        imgMemberIcon.ff_AlignHorizontal(type: ff_AlignType.BottomRight, referView: labName, size: nil, offset: CGPoint(x: 8, y: -2))
        
        // vip
        imgVipIcon.ff_AlignInner(type: ff_AlignType.BottomRight, referView: imgIcon, size: nil, offset: CGPoint(x: 6, y: 6))
        
        
    
    }
    
    
    // MARK: - 懒加载控件
    // 头像
    private lazy var imgIcon : UIImageView = UIImageView()
    
    // 姓名
    private lazy var labName : UILabel = UILabel(color: UIColor.darkGrayColor(), fontSize: 15.0)
    
    // 时间
    private lazy var labTime : UILabel = UILabel(color: UIColor.orangeColor(), fontSize: 11.0)
    
    // 来源
    private lazy var labSource : UILabel = UILabel(color: UIColor(white: 0.5, alpha: 1.0), fontSize: 11.0)
    
    // 会员图标
    private lazy var imgMemberIcon : UIImageView = UIImageView(image: UIImage(named: "common_icon_membership_level1"))
    
    // vip图标
    private lazy var imgVipIcon : UIImageView = UIImageView(image: UIImage(named: "avatar_enterprise_vip"))

 
}
