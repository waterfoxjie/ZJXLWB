//
//  StatusCell.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/4.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

// 控件间距
let statusCellContrMargin : CGFloat = 10.0

// MARK: - 定义协议
protocol StatusCellDelegate : NSObjectProtocol {
    
    // 选中链接文本
    func statusCellDidSelectedLinkText(text : String)
    
}

// MARK: - 定义标示符枚举
enum StatusCellID : String {

    case  StatusNormalCell = "StatusNormalCell"
    
    case  StatusForwardCell = "StatusForwardCell"
    
    // 定义一个静态函数
    // 根据是否有微博数据返回对应的ID
    static func cellID(status : XLStatuses) -> String {
    
        return status.retweeted_status == nil ? StatusCellID.StatusNormalCell.rawValue : StatusCellID.StatusForwardCell.rawValue
    }
    
}

/// 微博Cell
class StatusCell: UITableViewCell {
    
    // 定义代理
    weak var statusCellDelegate : StatusCellDelegate?

    // MARK: - 创建一个微博数据模型
    var status : XLStatuses? {
    
        didSet {
        
            // 设置模型内容
            topView.status = status
            
            let statusText = status?.text ?? ""
            
            contentLabView.attributedText = EmoticonPackage.emoticonText(statusText, font: contentLabView.font)
            
            pictureView.status = status
            
            // 设置配图视图的尺寸
            pictHeightCons?.constant = pictureView.bounds.size.height
            
            pictWidthCons?.constant = pictureView.bounds.size.width
            
            // 判断图片的高度约束是否有值，没有则此处也等于0
            pictTopCons?.constant = (pictureView.bounds.size.height) == 0 ? 0 : statusCellContrMargin
        }
    }
    
    // MARK: - 界面设置
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setUpUI()
        
    }

    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
        
    }
    
    /// 图片宽度约束、高度约束、顶部约束
    var pictWidthCons : NSLayoutConstraint?
    
    var pictHeightCons : NSLayoutConstraint?
    
    var pictTopCons : NSLayoutConstraint?
    
    // MARK: - 计算行高
    func rowheight(status : XLStatuses) -> CGFloat {
    
        self.status = status
        
        // 强行更新布局（所有控件的frame都会改变）
        layoutIfNeeded()
        
        // 返回底部视图的最大高度
        let rowHeight = CGRectGetMaxY(buttomView.frame)
        
        return rowHeight
        
    }
    

    // MARK: - 搭建界面
    func setUpUI() {
        
        // 添加底部分隔视图
        let sepView = UIView()
        
        sepView.backgroundColor = UIColor(white: 0.92, alpha: 1.0)
        
        // 添加控件
        contentView.addSubview(sepView)
        
        contentView.addSubview(topView)
        
        contentView.addSubview(contentLabView)
        
        contentView.addSubview(pictureView)
        
        contentView.addSubview(buttomView)
        
        // 设置布局
        /// 分隔视图
        sepView.ff_AlignInner(type: ff_AlignType.TopLeft, referView: contentView, size: CGSize(width: UIScreen.mainScreen().bounds.width, height: statusCellContrMargin) , offset: CGPoint(x: 0, y: 0))
        
        /// 头部View
        topView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: sepView, size: CGSize(width: UIScreen.mainScreen().bounds.width, height: 51) , offset: CGPoint(x: 0, y: 0))
        
        /// 中间文字View
        contentLabView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: topView, size: nil, offset: CGPoint(x: statusCellContrMargin * 0.5 * 3, y: statusCellContrMargin))
        
        // 设置控件宽度：主要和自动计算行高的属性联动使用
        contentView.addConstraint(NSLayoutConstraint(item: contentLabView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: -3 * statusCellContrMargin))
        
        /// 底部View
        buttomView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: pictureView, size: CGSize(width: UIScreen.mainScreen().bounds.width, height: 44), offset: CGPoint(x: -statusCellContrMargin * 0.5 * 3, y: statusCellContrMargin))
        
    }
    
    
    // MARK: - 懒加载控件
    // 顶部View
    private lazy var topView : StatusTopView = StatusTopView()
    
    // 中间微博内容View
    lazy var contentLabView : FFLabel = {
        
        let label = FFLabel(color: UIColor.darkGrayColor(), fontSize: 16.0)
        
        // 自己计算行高时，要设置文本折行的宽度
        label.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 3 *  statusCellContrMargin
        
        // 设置可以换行
        label.numberOfLines = 0
        
        // 设置label的代理
        label.labelDelegate = self
        
        return label
        
    }()
    
    // 图片显示View
    lazy var pictureView : StatusPictureView = StatusPictureView()
    
    // 底部View
    lazy var buttomView : StatusButtomView = StatusButtomView()
    
}

extension StatusCell : FFLabelDelegate {

    func labelDidSelectedLinkText(label: FFLabel, text: String) {
        
        print(text)
        
        // 判断text是否是HTTP链接
        if text.hasPrefix("http://") {
        
            statusCellDelegate?.statusCellDidSelectedLinkText(text)
        }
    }
}
