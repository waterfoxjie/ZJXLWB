//
//  MessageTabContr.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/7/30.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

class MessageTabContr: BaseTabContr {

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        norLoginView?.setupViewInfo(false, imgName: "visitordiscover_image_message", message: "登录后，别人评论你的微博，发给你的消息，都会在这里收到通知")

    }

}
