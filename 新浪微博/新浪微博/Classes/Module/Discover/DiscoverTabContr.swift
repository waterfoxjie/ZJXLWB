//
//  DiscoverTabContr.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/7/30.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

class DiscoverTabContr: BaseTabContr {

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        norLoginView?.setupViewInfo(false, imgName: "visitordiscover_feed_image_smallicon", message: "登录后，最新、最热微博尽在掌握，不再会与实事潮流擦肩而过")

        
    }

    
}
