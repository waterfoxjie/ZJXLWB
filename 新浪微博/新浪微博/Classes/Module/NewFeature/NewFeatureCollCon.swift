//
//  NewFeatureCollCon.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/3.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class NewFeatureCollCon: UICollectionViewController {
    
    // 定义一个图片的总数
    private let imgCount : Int = 4
    
    // 设置布局属性
    private let layout = FlowLayout()
    
    // MARK: - 调用init方法
    init() {
    
        // 调用父类的初始化方法
        super.init(collectionViewLayout: layout)
        
    }

    required init?(coder aDecoder: NSCoder) {
        
        // 本类禁止用 sb 开发 - 纯代码开发，考虑因素少，风险小
        // fatalError("init(coder:) has not been implemented")
        
        // 本类允许用 sb 开发，有些公司是混合开发！需要考虑的因素多，都需要测试才行！
        super.init(coder: aDecoder)
        
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()

        // 可重用cell
        self.collectionView!.registerClass(NewFeatureCell.self, forCellWithReuseIdentifier: reuseIdentifier)

    }


    // MARK: - UICollectionViewDataSource  数据源方法，返回有多少个cell
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imgCount
    }

    // 每个cell有什么
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! NewFeatureCell
    
        cell.imgIndex = indexPath.item
    
        return cell
    }
    
    // MARK: - 完成显示 cell - indexPath 是之前消失 cell 的 indexPath
    override func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        // 获取当前显示的indexPath
        let index = collectionView.indexPathsForVisibleItems().last!
        
        // 判断是否是最后一张图片
        if index.item == imgCount - 1 {
        
            // 拿到对应得cell并播放动画
            let cell = collectionView.cellForItemAtIndexPath(index) as! NewFeatureCell
            
            cell.btnStartAnim()
            
        }
        
    }
}


// MARK: - 自定义cell
class NewFeatureCell : UICollectionViewCell {
    
    // 定义一个图片索引的属性
    private var imgIndex : Int = 0 {
        
        didSet {
            
            // 根据索引值设置图片
            imgBackground.image = UIImage(named:"new_feature_\(imgIndex + 1)")
            
            // 设置按钮不可见
            btnStart.hidden = true
        }
    }
    
    
    // MARK: - 按钮点击事件
    func btnClick() {
        
        // 发送通知，跳转到主界面
        NSNotificationCenter.defaultCenter().postNotificationName(ZJRootViewControllerSwitchNotification, object: true)
        
    }
    
    
    // MARK: - 重写init（frame）方法
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        newFeatureUI()
        
    }

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        newFeatureUI()
        
    }
    
    
    // MARK: - 设置按钮动画
    private func btnStartAnim() {
        
        // 设置按钮可见
        btnStart.hidden = false
    
        // 修改按钮的transform
        btnStart.transform = CGAffineTransformMakeScale(0, 0)
        
        // 动画开始时，设置按钮不能与用户交互
        btnStart.userInteractionEnabled = false
        
        // 动画
        UIView.animateWithDuration(2.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 5.0, options: UIViewAnimationOptions(rawValue: 0), animations: { () -> Void in
            
            // 恢复按钮的形状
            self.btnStart.transform = CGAffineTransformIdentity
            
            }) { (_) -> Void in
                
                self.btnStart.userInteractionEnabled = true
        }
    }
    
    
    // MARK: - 准备新特性界面
    private func newFeatureUI() {
    
        // 添加控件
        contentView.addSubview(imgBackground)
        
        contentView.addSubview(btnStart)
        
        // 自动布局 : 平铺
        imgBackground.ff_Fill(contentView)
        
        // 按钮自动布局 : 中下(内部)
        // type：对齐的样式   referView：参照视图   size：控件大小   offset：偏移量
        btnStart.ff_AlignInner(type: ff_AlignType.BottomCenter, referView: contentView, size: nil, offset: CGPoint(x: 0, y: -180))
        
    }

    // MARK: - 懒加载控件
    // 背景图
    private lazy var imgBackground = UIImageView()
    
    // 最后一页显示的按钮
    private lazy var btnStart : UIButton = {
        
        let btn = UIButton()
        
        // 设置图片
        btn.setBackgroundImage(UIImage(named: "new_feature_finish_button"), forState: UIControlState.Normal)
        
        btn.setBackgroundImage(UIImage(named: "new_feature_finish_button_highlighted"), forState: UIControlState.Highlighted)
        
        btn.setTitle("开始体验", forState: UIControlState.Normal)
        
        btn.addTarget(self, action: "btnClick", forControlEvents: UIControlEvents.TouchUpInside)

        btn.sizeToFit()
        
        return btn
        
    }()
}


// MARK: - 自定义布局
private class FlowLayout : UICollectionViewFlowLayout {
    
    // 重写prepareLayout方法，这个方法是布局cell的时候，系统会自动调用一次这个方法，当布局没有失效时，这个方法只会被调用一次
    private override func prepareLayout() {
        
        // 设置cell的大小为屏幕的大小
        itemSize = collectionView!.bounds.size
        
        // 设置间距为0
        minimumInteritemSpacing = 0
        
        minimumLineSpacing = 0
        
        // 设置滚动方向
        scrollDirection = UICollectionViewScrollDirection.Horizontal
        
        // 设置分页
        collectionView?.pagingEnabled = true
        
        collectionView?.showsHorizontalScrollIndicator = false
        
        collectionView?.bounces = false
        
    }

    
}



