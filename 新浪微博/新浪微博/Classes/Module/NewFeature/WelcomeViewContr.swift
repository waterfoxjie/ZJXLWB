//
//  WelcomeViewContr.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/3.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit
import SDWebImage

class WelcomeViewContr: UIViewController {
    
    // 定义一个底部约束属性
    private var imgIconButtomCon : NSLayoutConstraint?

    override func viewDidLoad() {
        
        super.viewDidLoad()

        welcome()
        
        // 加载用户头像
        if let urlString = UserAccount.loadAccount()?.avatar_large {
        
            // 赋值
            ImgIcon.sd_setImageWithURL(NSURL(string: urlString))
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        // 设置作画
        // 取到屏幕的大小之后，减去底部约束，就是动画要执行的高度
        // 修改约束（注意：修改约束之后不会立即生效）
        imgIconButtomCon?.constant = -UIScreen.mainScreen().bounds.height - imgIconButtomCon!.constant + 20
        
        // 动画  usingSpringWithDamping：弹力系数   initialSpringVelocity：起始速度
        UIView.animateWithDuration(2.0, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 10.0, options: UIViewAnimationOptions(rawValue: 0), animations: { () -> Void in
            
            // 强制更新约束
            self.view.layoutIfNeeded()
            
            }) { (_) -> Void in
                
                // 发送通知，跳转到主界面
                NSNotificationCenter.defaultCenter().postNotificationName(ZJRootViewControllerSwitchNotification, object: true)
                
        }
    }
    
    
    // MARK: - 准备界面函数
    private func welcome() {
    
        // 添加控件
        view.addSubview(ImgBackground)
        
        view.addSubview(ImgIcon)
        
        view.addSubview(labTitle)
        
        // 设置自动布局
        // 背景图：适应在不同的屏幕上能够适配
        ImgBackground.ff_Fill(view)
        
        // 头像
        let cons = ImgIcon.ff_AlignInner(type: ff_AlignType.BottomCenter, referView: view, size: CGSize(width: 90, height: 90), offset: CGPoint(x: 0, y: -160))
        
        // 记录底部约束 最后一个约束为底部约束
        imgIconButtomCon = ImgIcon.ff_Constraint(cons, attribute: NSLayoutAttribute.Bottom)
        
        // label
        labTitle.ff_AlignVertical(type: ff_AlignType.BottomCenter, referView: ImgIcon, size: nil, offset: CGPoint(x: 0, y: 15))
        
    }
    
    
    // MARK: - 懒加载控件
    // 背景图
    private lazy var ImgBackground : UIImageView = UIImageView(image: UIImage(named: "ad_background"))
    
    // 头像
    private lazy var ImgIcon : UIImageView = {
    
        let img = UIImageView(image: UIImage(named: "avatar_default_big"))
        
        // 设置头像为圆形
        img.layer.masksToBounds = true
        
        img.layer.cornerRadius = 45
        
        return img
        
    }()
    
    // label
    private lazy var labTitle : UILabel = {
    
        let lab = UILabel()
        
        lab.text = "欢迎归来"
        
        lab.font = UIFont.systemFontOfSize(15)
        
        lab.textColor = UIColor.grayColor()
        
        lab.sizeToFit()
        
        return lab
        
    }()


    



    
}
