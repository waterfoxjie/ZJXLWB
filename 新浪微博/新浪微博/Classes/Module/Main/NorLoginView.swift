//
//  NorLoginView.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/7/30.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

// 创建协议
protocol NorLoginViewDelegate : NSObjectProtocol {

    // 要实现两个方法
    // 注册
    func registerBtnClick()
    
    // 登录
    func loginBtnChick()
    
}


class NorLoginView: UIView {
    
    // 创建代理
    // 一定要用weak，防止循环引用
    weak var delegate : NorLoginViewDelegate?

    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        setupUI()
        
    }

    required init?(coder aDecoder: NSCoder) {
     
        // fatalError("init(coder:) has not been implemented")
        
        super.init(coder: aDecoder)
        
        setupUI()
        
    }
    
    // MARK: - 开始动画
    private func startAnimation() {
    
        let anim = CABasicAnimation(keyPath: "transform.rotation")
        
        // 设置动画旋转的角度、时间、次数
        // 角度
        anim.toValue = 2 * M_PI
        
        anim.repeatCount = MAXFLOAT
        
        anim.duration = 20.0
        
        // removedOnCompletion：默认为YES，代表动画执行完毕后就从图层上移除，图形会恢复到动画执行前的状态
        // 设置为false表示执行完毕之后不从图层上移除
        anim.removedOnCompletion = false
        
        // 添加动画
        iconImgView.layer.addAnimation(anim, forKey: nil)
        
    }
    
    // MARK: - 设置视图信息
    // 外面的类也可以调用这个方法
    func setupViewInfo(isHome : Bool , imgName : String , message : String) {
    
        iconImgView.image = UIImage(named: imgName)
        
        messageLab.text = message
        
        // 遮罩、小房子显示由是否是首页这个布尔值决定
        maskIconView.hidden = !isHome
        
        // 首页则调动画的方法，其他页面则让遮罩不显示
        isHome ? startAnimation() : (hourseImgView.hidden = !isHome)
        
    }
    
    // MARK: - 按钮点击时间
    func register() {
    
        // 调用代理方法
        delegate?.registerBtnClick()
        
    }
    
    // 登录
    func login() {
    
        delegate?.loginBtnChick()
    }
    
    
    // MARK: - 添加控件、自动布局
    // 创建界面上的控件，对控件进行自动布局
    private func setupUI() {
    
        // 添加控件到界面上
        addSubview(iconImgView)
        
        addSubview(maskIconView)
        
        addSubview(hourseImgView)
        
        addSubview(messageLab)
        
        addSubview(registerBtn)
        
        addSubview(loginBtn)
        
        // 图标
        iconImgView.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: iconImgView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: iconImgView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: -65))
        
        // 小房子
        hourseImgView.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: hourseImgView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: iconImgView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: hourseImgView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: iconImgView, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: -8))
        
        // label
        messageLab.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: messageLab, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: iconImgView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: messageLab, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: iconImgView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 15))
        
        // 设置宽度
        addConstraint(NSLayoutConstraint(item: messageLab, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 230))
        
        // 注册按钮
        registerBtn.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: messageLab, attribute: NSLayoutAttribute.Left, multiplier: 1.0, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: messageLab, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 15))
        
        addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 100))
        
        addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 35))
        
        // 登录按钮
        loginBtn.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: loginBtn, attribute: NSLayoutAttribute.Right, relatedBy: NSLayoutRelation.Equal, toItem: messageLab, attribute: NSLayoutAttribute.Right, multiplier: 1.0, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: loginBtn, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: messageLab, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 15))
        
        addConstraint(NSLayoutConstraint(item: loginBtn, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 100))
        
        addConstraint(NSLayoutConstraint(item: loginBtn, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 35))
        
        // 遮罩
        maskIconView.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[subview]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["subview" : maskIconView]))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[subview]-(-40)-[regButton]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["subview" : maskIconView,"regButton" : registerBtn]))
        
        // 设置背景图片
        backgroundColor = UIColor(white: 237.0 / 255.0, alpha: 1.0)
        
        
    }
    
    // MARK: - 懒加载控件
    // 图标ImageView
    lazy var iconImgView : UIImageView = {
    
        // 实例化
        let iconImg = UIImageView()
        
        return iconImg
        
    }()
    
    // 遮罩控件
    lazy var maskIconView : UIImageView = {
        
        let mv = UIImageView(image: UIImage(named: "visitordiscover_feed_mask_smallicon"))
        
        return mv
    
    }()
    
    // 小房子
    lazy var hourseImgView : UIImageView = {
    
        let hv = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
        
        return hv
        
    }()
    
    // 中间的label
    lazy var messageLab : UILabel = {
    
        let ml = UILabel()
        
        // 设置文字大小、颜色
        ml.textColor = UIColor.grayColor()
        
        ml.font = UIFont.systemFontOfSize(14)
        
        // 设置换行
        ml.numberOfLines = 0
        
        return ml
        
    }()
    
    // 注册按钮
    lazy var registerBtn : UIButton = {
    
        let leftBtn = UIButton()
        
        leftBtn.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Normal)
        
        leftBtn.setTitleColor(UIColor.orangeColor(), forState: UIControlState.Normal)
        
        leftBtn.setTitle("注册", forState: UIControlState.Normal)
        
        leftBtn.titleLabel?.font = UIFont.systemFontOfSize(15)
        
        // 设置点击事件
        leftBtn.addTarget(self, action:"register" , forControlEvents: UIControlEvents.TouchUpInside)
        
        return leftBtn
        
    }()
    
    // 登录按钮
    lazy var loginBtn : UIButton = {
        
        let loginBtn = UIButton()
        
        loginBtn.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Normal)
        
        loginBtn.setTitleColor(UIColor.orangeColor(), forState: UIControlState.Normal)
        
        loginBtn.setTitle("登录", forState: UIControlState.Normal)
        
        loginBtn.titleLabel?.font = UIFont.systemFontOfSize(15)
        
        // 设置点击事件
        loginBtn.addTarget(self, action:"login", forControlEvents: UIControlEvents.TouchUpInside)
        
        return loginBtn
        
        }()
    
    
}
