//
//  BaseTabContr.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/7/30.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

// 遵守协议
class BaseTabContr: UITableViewController , NorLoginViewDelegate{

    // 创建一个记录用户登录的常量
    // 微博用户账号不等于空的时候，直接跳转
    // let userLogin = UserAccount.loadAccount() != nil
    let userLogin = UserAccount.userLogin
    
    var norLoginView : NorLoginView?
    
    override func loadView() {
        
        print(userLogin)
        // 根据用户是否登录判断是否替换根视图
        // 如果用户没有登录，则调用下面定义的访客视图，如果登录了，就调用主函数中创建的
        userLogin ? super.loadView() : setupVisitorView()
        
    }
    
    // 设置访客视图
    private func setupVisitorView() {
        
        norLoginView = NorLoginView()
    
        // 设置代理
        norLoginView?.delegate = self
        
        // view是一个强制解包
        view = norLoginView
        
        // 设置状态栏
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: UIBarButtonItemStyle.Plain, target: self, action: "registerBtnClick")
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: UIBarButtonItemStyle.Plain, target: self, action: "loginBtnChick")
        
    }
    
    // 实现协议中得方法
    func registerBtnClick() {
        
        print("注册")
    }
    
    func loginBtnChick() {
        
        // 创建一个navigation控制器
        let nav = UINavigationController(rootViewController: OAuthViewController())
        
        // module一个控制器
        // module 就是点击之后弹出一个控制器的过程，释放使用dismiss
        presentViewController(nav, animated: true, completion: nil)
        
        print("登录")
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
