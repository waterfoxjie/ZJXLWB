//
//  MainViewContr.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/7/30.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

class MainViewContr: UITabBarController {

    override func viewDidLoad() {
       
        super.viewDidLoad()
        
        addChildViewControllers()
        
    }
    
    // 调用这个方法之前按钮已经创建好了，此时就可以设置按钮的位置了
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        setupMiddleBtn()
        
    }
    
    // MARK:- 创建一个方法，用于创建多个视图，只是创建控制器，button并没有创建
    private func addChildViewControllers() {
        
        addChildViewController(HomeTabContr(), title: "首页", imgName: "tabbar_home")
        
        addChildViewController(MessageTabContr(), title: "消息", imgName: "tabbar_message_center")
        
        // 创建一个系统自带的控制器，占位
        addChildViewController(UIViewController())
        
        addChildViewController(DiscoverTabContr(), title: "发现", imgName: "tabbar_discover")
        
        addChildViewController(ProfileTabContr(), title: "我", imgName: "tabbar_profile")
        
    }

    // MARK:- 创建视图控制器
    // 优化：是程序更加灵活，添加三个参数，调用这个方法就可以根据需求创建对应的控制器
    private func addChildViewController(vc : UITableViewController , title : String , imgName : String) {
        
        vc.title = title
        
        // 设置navigationController
        let nav = UINavigationController(rootViewController: vc)
        
        vc.tabBarItem.image = UIImage(named: imgName)
        
        addChildViewController(nav)
        
    }
    
    // MARK:- 按钮单机事件
    // 运行循环监听到事件后，向 VC 发送消息，动态执行 方法，因此不能设置为 private
    func middleBtnChick() {
        
        // 判断用户是否登录
        // 创建一个发布微博控制器
        let con = UserAccount.userLogin ? ComposeViewContr() : OAuthViewController()
        
        // 创建一个navigationController
        let nav = UINavigationController(rootViewController: con)
        
        presentViewController(nav, animated: true, completion: nil)
        
        print("按钮被点击了....")
    
    }
    
    
    // MARK:- 设置按钮的位置
    private func setupMiddleBtn() {
    
        let btnW = tabBar.bounds.width / CGFloat(viewControllers!.count)
        
        let rect = CGRect(x: 0, y: 0, width: btnW, height: tabBar.bounds.height)
        
        middleBtn.frame = CGRectOffset(rect, btnW * 2 , 0)
        
    }
    
    // MARK:- 懒加载按钮
    lazy var middleBtn : UIButton = {
    
        let btn = UIButton()
        
        // 设置背景图片,普通状态以及高亮状态
        btn.setImage(UIImage(named: "tabbar_compose_icon_add"), forState: UIControlState.Normal)
        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button"), forState: UIControlState.Normal)
        
        btn.setImage(UIImage(named: "tabbar_compose_icon_add_highlighted"), forState: UIControlState.Highlighted)
        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), forState: UIControlState.Highlighted)
        
        // 添加一个监听方法
        btn.addTarget(self, action: "middleBtnChick", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.tabBar.addSubview(btn)
        
        return btn
        
    }()


}
