//
//  ProfileTabContr.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/7/30.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

class ProfileTabContr: BaseTabContr {

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        norLoginView?.setupViewInfo(false, imgName: "visitordiscover_image_profile", message: "登录后，你的微博、相册、个人资料会显示在这里，展示给别人")

        
    }

}
