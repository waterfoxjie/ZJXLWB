//
//  SQLiteManager.swift
//  FMDB
//
//  Created by waterfoxjie on 15/9/2.
//  Copyright © 2015年 zj. All rights reserved.
//

import Foundation

// 默认的数据库文件名以db结尾，但是这样容易被发现
// SQLite 公开的版本中是不支持加密的，所以可以将数据库的名字设置为常见的名字
private let dbName = "status.db"

class SQLiteManager {

    // 创建单例
    static let sharedManager = SQLiteManager()
    
    // 队列能够保证数据的安全，内部有一个串行队列
    let queue : FMDatabaseQueue
    
    // MARK: - 打开数据库
    init() {
    
        // 获取路径
        let path = (NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).last! as NSString).stringByAppendingPathComponent(dbName)
        
        print(path)
        
        // 创建数据库队列，数据库不存在的话会新建一个，存在的话会直接打开数据库
        // 后续所有相关的数据操作，都通过queue操作
        queue = FMDatabaseQueue(path: path)
        
        creatTable()
        
    }
    
    
    func creatTable() {
        
        // 获取地址
        let path = NSBundle.mainBundle().pathForResource("tables.sql", ofType: nil)!
    
        let sql = try! String(contentsOfFile: path)
        
        // 第二个参数是回滚
        queue.inTransaction { (db, roolBack) -> Void in
            
            /**
            * executeStatements ：执行很多sql语句
            * executeQuery ：执行查询
            * executeUpdate ：执行单条SQL，除了SELECT之外的语句都可以用这个函数
            */
            if db.executeStatements(sql) {
            
                print("创建数据表成功")
            
            } else {
            
                print("创建数据表失败")
            }
        }
    }
    
    
    
}
