-- 创建“个人表”
CREATE TABLE IF NOT EXISTS "T_Person" (
"id" INTEGER,
"name" TEXT,
"age" INTEGER,
"height" REAL,
"title" TEXT
);

-- 创建“公司表”
CREATE TABLE IF NOT EXISTS "T_Company" (
"companyId" INTEGER,
"companyName" TEXT
);