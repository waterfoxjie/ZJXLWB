//
//  UIBarButtonItem+Extension.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/9.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    
    convenience init(imageName : String , target :AnyObject? , action : String?) {
        
        // 定义按钮
        let btn = UIButton()
        
        // 设置不同状态的图片
        btn.setImage(UIImage(named: imageName), forState: UIControlState.Normal)
        
        btn.setImage(UIImage(named: imageName + "_highlighted"), forState: UIControlState.Highlighted)
        
        // 设置监听方法
        // 判断是否有方法
        if let acctionName = action {
            
            btn.addTarget(target, action: Selector(acctionName) , forControlEvents: UIControlEvents.TouchUpInside)
        }
        
        
        btn.sizeToFit()
        
        self.init(customView : btn)
        
    }

}
