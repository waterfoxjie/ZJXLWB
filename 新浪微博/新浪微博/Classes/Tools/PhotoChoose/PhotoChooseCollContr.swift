//
//  PhotoChooseCollContr.swift
//  照片选择器
//
//  Created by waterfoxjie on 15/8/17.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

// 可重用cellID
private let PhotoChooseCell = "PhotoChooseCell"

// 最大能够设置的图片数量
private let PhotoChooseCellCount = 9

class PhotoChooseCollContr: UICollectionViewController , PhotoSelectorViewCellDelegate {
    
    // MARK: - 图片数组属性
    lazy var photos : [UIImage] = [UIImage]()
    
    // 添加一个照片索引
    private var currenIndex = 0
    
    // 构造函数
    init() {
    
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
        
    }

    required init?(coder aDecoder: NSCoder) {
       
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.collectionView?.backgroundColor = UIColor.whiteColor()
        
        // Register cell classes
        self.collectionView!.registerClass(PhotoChooseViewCell.self, forCellWithReuseIdentifier: PhotoChooseCell)
        
        // 定义布局属性
        let layout = collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        
        // 设置cell的大小
        layout.itemSize = CGSize(width: 80, height: 80)
        
        // 设置cell之间的边距
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
    }

    // MARK: - 数据源方法
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // 设置cell数量
        let count = (photos.count == PhotoChooseCellCount) ? photos.count : photos.count + 1
        
        return count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(PhotoChooseCell, forIndexPath: indexPath) as! PhotoChooseViewCell
    
        cell.backgroundColor = UIColor.redColor()
        
        // 设置图片，需要加判断，判断数组是否会越界
        cell.image = indexPath.item < photos.count ? photos[indexPath.item] : nil
        
        // 设置代理
        cell.photoCellDelegate = self
    
        return cell
    }
    
    // MARK: - 实现代理方法
    // 如果协议是 private，在实现函数的时候，同样也是 private 的
    // 选择照片
    private func choosePhoto(cell: PhotoChooseViewCell) {
        
        /**
        PhotoLibrary(默认类型) 照片库－所有的照片，包括使用 iTunes/ iPhoto 同步的照片，不允许删除
        SavedPhotosAlbum    胶卷，就是使用摄像头拍摄的照片，从其他应用程序保存的图片，可以删除
        Camera              相机
        */
        if !UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
        
            print("无法使用照片库")
            
            return
        }
        
        // 记录索引
        let indexPath = collectionView?.indexPathForCell(cell)
        
        currenIndex = indexPath!.item
        
        // 实例化照片选择控制器
        let picker = UIImagePickerController()
        
        // 监听选中照片事件
        picker.delegate = self
        
//        // 设置允许编辑 － 会多一个窗口，让用户缩放照片
//        // 在实际开发中，如果让用户从照片库选择头像，非常重要！
//        // 好处：1. 正方形，2. 图像会小
//        picker.allowsEditing = true
        
        presentViewController(picker, animated: true, completion: nil)
        
    }
    
    // 删除照片
    private func deletePhoto(cell: PhotoChooseViewCell) {
        
        // 找到用户点击的cell
        let indexPath = collectionView?.indexPathForCell(cell)
        
        // 删除数组中对应对应的项
        photos.removeAtIndex(indexPath!.item)
        
        // 刷新数据
        collectionView?.reloadData()
        
    }
    
}

// MARK: - 实现picker代理方法
extension PhotoChooseCollContr : UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    // 注意：一旦实现了代理方法，就需要程序员自己关闭控制器
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        let img = image.scallImage(300)
        
        // 判断用户选择的btn中是否有图像，没有则添加新的，有则进行修改
        if currenIndex == photos.count {
        
            // 等于照片的数量，说明要添加新的图片 ， 将图片添加到数组中
            photos.append(img)

        } else {
        
            // 小于说明是有图片的，此时进行修改图片的操作
            photos[currenIndex] = img
        }
        
        print(img)
        
        
        // 刷新视图
        collectionView?.reloadData()
        
        dismissViewControllerAnimated(true, completion: nil)
        
    }
}



// MARK: - 定义协议
// 提示：如果元素之间有非常明显的包含和被被包含的关系，建议使用代理
private protocol PhotoSelectorViewCellDelegate : NSObjectProtocol {

    // 选择照片方法
    func choosePhoto(cell : PhotoChooseViewCell)
    
    // 删除照片
    func deletePhoto(cell : PhotoChooseViewCell)
    
}


// MARK: - 自定义cell
private class PhotoChooseViewCell : UICollectionViewCell {
    
    // 设置代理属性
    weak var photoCellDelegate : PhotoSelectorViewCellDelegate?
    
    // 定义一个图片属性
    var image : UIImage? {
    
        didSet {
        
            // 设置按钮图片 -- 判断图像是否为空
            if image == nil {
            
                // 显示默认的图片
                btnPhoto.setImage("compose_pic_add")
                
            } else {
            
                btnPhoto.setImage(image, forState: UIControlState.Normal)
                
            }
            
            // 设置删除按钮的隐藏
            // 没有图片的时候隐藏
            btnRemove.hidden = (image == nil)
        }
    }
    
    // MARK: - 按钮监听事件
    // 类是private类型时，如果真的不希望开放这个类给其他对象访问，使用一个关键字 @objc
    // 可以保证即使类是私有的，运行循环同样可以通过消息发送找到该函数
    // 选择照片
    @objc func btnChoosePhotoChick() {
    
        photoCellDelegate?.choosePhoto(self)
    }
    
    // 删除照片
    @objc func btnDeletePhotoChick() {
    
        photoCellDelegate?.deletePhoto(self)
    }
    
    // MARK: - 构造函数
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        setUpUI()
    }

    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - 设置界面
    private func setUpUI() {
    
        // 添加界面
        addSubview(btnPhoto)
        
        addSubview(btnRemove)
        
        // 自动布局
        btnPhoto.translatesAutoresizingMaskIntoConstraints = false
        
        btnRemove.translatesAutoresizingMaskIntoConstraints = false
        
        let dict = ["photo" : btnPhoto , "remove" : btnRemove]
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[photo]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[photo]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        // 水平向右居中
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[remove]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        // 垂直向上居中
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[remove]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        // 添加按钮监听方法
        btnPhoto.addTarget(self, action: "btnChoosePhotoChick", forControlEvents: UIControlEvents.TouchUpInside)
        
        btnRemove.addTarget(self, action: "btnDeletePhotoChick", forControlEvents: UIControlEvents.TouchUpInside)
        
        // 修改照片的填充模式
        btnPhoto.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
        
    }

    // MARK: - 懒加载控件
    private lazy var btnPhoto : UIButton = UIButton(imgName: "compose_pic_add")
    
    private lazy var btnRemove : UIButton = UIButton(imgName: "compose_photo_close")
    
    
    
}
