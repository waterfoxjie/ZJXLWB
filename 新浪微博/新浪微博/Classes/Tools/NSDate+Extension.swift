//
//  NSDate+Extension.swift
//  时间功能模块
//
//  Created by waterfoxjie on 15/8/25.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

extension NSDate {

    // 将新浪的日期字符串转为日期
    class func sinaDate(string : String) -> NSDate? {
        
        // 转换日期
        let dateFormatter = NSDateFormatter()
        
        // 一定要指定区域
        dateFormatter.locale = NSLocale(localeIdentifier: "en")
        
        // 日期格式
        dateFormatter.dateFormat = "EEE MMM dd HH:mm:ss zzz yyyy"
        
        // 转换成日期
        let date = dateFormatter.dateFromString(string)
        
        return date

    }
    
    /**
    刚刚(一分钟内)
    X分钟前(一小时内)
    X小时前(当天)
    昨天 HH:mm(昨天)
    MM-dd HH:mm(一年内)
    yyyy-MM-dd HH:mm(更早期)
    */
    // 给一个属性，用于打印日期信息
    // 苹果日历类 ：提供了十分丰富的日期转换函数
    var dateDesctiption : String {
        
        // 设置日式显示格式
        var fmtString = " HH:mm"
        
        // 获取当前的日期
        let cal = NSCalendar.currentCalendar()
        
        // 判断是否是今天
        if cal.isDateInToday(self) {
            
            // 今天的要使用当前日期和系统时间进行比较
            let delta = Int(NSDate().timeIntervalSinceDate(self))
            
            if delta < 60 {
            
                return "刚刚"
            }
            
            if delta < 3600 {
            
                return "\(delta / 60) 分钟前"
            }
        
            return "\(delta / 3600) 小时前"
        }
        
        // 判断是否是昨天
        if cal.isDateInYesterday(self) {
            
            fmtString = "昨天" + fmtString

        } else {
        
            fmtString = "MM-dd" + fmtString
            // 获取年度
            // 这个方法用于获取年度数值
            // print(cal.component(NSCalendarUnit.Year, fromDate: self))
            // 用户判断是否是一个完整的自然年（即是否满一年的时间，是则返回1，不是则返回0）
            let coms = cal.components(NSCalendarUnit.Year, fromDate: self, toDate: NSDate(), options: NSCalendarOptions(rawValue: 0))
            
            if coms.year > 0 {
                
                fmtString = "yyyy-" + fmtString
            }
        }
        
        let df = NSDateFormatter()
        
        // 指定区域
        df.locale = NSLocale(localeIdentifier: "en")
        
        df.dateFormat = fmtString
    
        return df.stringFromDate(self)
    }
    
    
}
