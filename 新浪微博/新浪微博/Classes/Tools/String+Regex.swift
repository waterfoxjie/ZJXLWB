//
//  String+Regex.swift
//  正则表达式
//
//  Created by waterfoxjie on 15/8/25.
//  Copyright © 2015年 zj. All rights reserved.
//

import Foundation

extension String {
    
    // 提取字符串中href链接内容
    /// 返回的(link : String , text : String)这个叫做元祖，能够保证一次返回多个值
    func hrefLink() -> (link : String? , text : String?) {
    
        // 找到特征字符
        let pattern = "<a href=\"(.*?)\".*?>(.*?)</a>"
        
        // 定义匹配字符
        // 设置匹配选项 ：DotMatchesLineSeparators（让 . 能匹配换行符）通常用于网络爬虫
        // 此处需要强try
        let regual = try! NSRegularExpression(pattern: pattern, options: NSRegularExpressionOptions.DotMatchesLineSeparators)
        
        
        // 开始匹配
        /// 取值之前必须进行判断，判断是否已经匹配到了相应的内容
        // firstMatchInString : 找到第一个符合要求的，返回 ：NSTextCheckingResult?类型
        if let result = regual.firstMatchInString(self, options: NSMatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) {
        
            // 获取结果
            // numberOfRanges属性 ：找到的范围数量
            // rangeAtIndex函数 ：按照idx获取range
            let r1 = result.rangeAtIndex(1)
            let r2 = result.rangeAtIndex(2)
            
            let link = (self as NSString).substringWithRange(r1)
            let text = (self as NSString).substringWithRange(r2)
            
            return (link , text)

        }
        
        return (nil , nil)
        
    }

}
