//
//  EmoticonAttachment.swift
//  表情键盘
//
//  Created by waterfoxjie on 15/8/11.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

class EmoticonAttachment: NSTextAttachment {
    
    // 记录表情符号
    var chs : String?
    
    // MARK: - 创建表情文字字符串
    class func imgText(emoticon : Emoticon , font : UIFont) -> NSAttributedString {
    
        // 1、创建图片属性字符串
        // 添加附件
        let attachment = EmoticonAttachment()
        
        // 记录表情属性
        attachment.chs = emoticon.chs
        
        // 设置附件的图片(通过获取图片的完整路径)
        attachment.image = UIImage(contentsOfFile: emoticon.imagePath)
        
        // MARK: - 设置边界（解决插入的图片过大的bug） lineHeight表示文本框中文字的线高
        let h = font.lineHeight
        
        // 在scrollView中，bounds的原点相当于contentOffset
        attachment.bounds = CGRect(x: 0, y: -4, width: h, height: h)
        
        // 创建图片属性字符串
        let imgText = NSMutableAttributedString(attributedString: NSAttributedString(attachment: attachment))
        
        // MARK: - 添加文本属性（解决后面的图标变小的bug）
        // name : 属性名称   value : 属性值
        imgText.addAttribute(NSFontAttributeName, value: font, range: NSRange(location: 0, length: 1))
        
        return imgText
    }

}
