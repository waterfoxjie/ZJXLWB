//
//  UITextView+Emoticon.swift
//  表情键盘
//
//  Created by waterfoxjie on 15/8/12.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

extension UITextView {

    // 定义一个计算型属性
    var emoticonText : String {
        
        // 属性文本是分段保存的，保存图片时，dict中有NSAttachment这个属性
        // 拿到每一个分段
        let attString = attributedText
        
        var strM = String()
        
        // 遍历
        attString.enumerateAttributesInRange(NSRange(location: 0, length: attString.length), options: NSAttributedStringEnumerationOptions(rawValue: 0)) { (dict, range, _) -> Void in
            
            // 判断是否是表情图片
            if let attachment = dict["NSAttachment"] as? EmoticonAttachment {
                
                strM += attachment.chs!
                
            }
            else {
                
                // 使用range获取文本的内容
                let str = (attString.string as NSString).substringWithRange(range)
                
                strM += str
            }
        }
        
        return strM
    }
    
    
    // 将图像插入文本的函数
    // 注意：如果在循环引用的闭包中调用函数，尽管函数中不用出现 `self`，但是仍然会出现循环引用
    // 1. 控制器持有 键盘控制器
    // 2. 键盘控制器持有 闭包
    // 3. 闭包对控制器进行强引用
    func insertEmoticon(emoticon : Emoticon) {
        
        // MARK: - 删除按钮功能
        if emoticon.removeEmoticon {
        
            deleteBackward()
            
            return
        }
        
        // 插入emoji表情
        // 判断表情是否为空
        if emoticon.emoji != nil {
            
            // UITextRange 仅在 UITextView 中有
            // 一个UITextRange对象代表一个字符在文本容器的范围，换句话说，它确定一个起始索引和结束索引一个字符串，支持一个文本输入对象。
            replaceRange(selectedTextRange!, withText: emoticon.emoji!)
            
            return
        }
        
        // 判断是否有表情图片
        if emoticon.chs != nil {
            
            // 1、创建图片属性字符串
            let imgText = EmoticonAttachment.imgText(emoticon, font: font!)
            
            // 2、将图片文字插入到textView中
            // 获取属性文本
            let attString = NSMutableAttributedString(attributedString: attributedText)
            
            // 将图片插入文字
            attString.replaceCharactersInRange(selectedRange, withAttributedString: imgText)
            
            // 3、使用可变属性文本替换文本视图的内容
            // MARK: - 记录光标的位置
            let range = selectedRange
            
            attributedText = attString
            
            // 恢复光标的位置（可解决插入表情之后光标移到最后的bug）,光标移到当前位置的后面
            selectedRange = NSRange(location: range.location + 1, length: 0)
            
            // 手动调用代理方法
            delegate?.textViewDidChange!(self)
            
        }
        
    }


}
