//
//  UILabel+Extension.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/5.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

extension UILabel {
    
    convenience init(color : UIColor , fontSize : CGFloat) {
    
        self.init()
        
        textColor = color
        
        font = UIFont.systemFontOfSize(fontSize)
        
    }
}
