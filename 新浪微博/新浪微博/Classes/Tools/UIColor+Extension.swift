//
//  UIColor+Extension.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/19.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

extension UIColor {

    // 生成随机颜色
    class func randomColor() -> UIColor {
    
        return UIColor(red: randomNum(), green: randomNum(), blue: randomNum(), alpha: 1.0)
    }
    
    // 生成随机数
    private class func randomNum() -> CGFloat {
    
        // arc4random_uniform(256) ： 生成0 ~ 255
        return CGFloat(arc4random_uniform(256)) / 255
    }
    
}

