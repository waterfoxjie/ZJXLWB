//
//  UIButton+Extension.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/5.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

extension UIButton {
    
    convenience init(title : String , fontSize : CGFloat = 14 , color : UIColor = UIColor.whiteColor() , backColor : UIColor = UIColor.grayColor()) {
        
        self.init()
        
        setTitle(title, forState: UIControlState.Normal)
        
        setTitleColor(color, forState: UIControlState.Normal)
        
        backgroundColor = backColor
        
        titleLabel?.font = UIFont.systemFontOfSize(fontSize)
        
    }

    convenience init(title : String , imgName : String , fontSize : CGFloat = 13 , color : UIColor = UIColor(white: 0.5, alpha: 1.0)) {
    
        self.init()
        
        setTitle(title, forState: UIControlState.Normal)
        
        setImage(UIImage(named: imgName), forState: UIControlState.Normal)
        
        setTitleColor(color, forState: UIControlState.Normal)
        
        titleLabel?.font = UIFont.systemFontOfSize(fontSize)
        
    }
    
    convenience init(imgName : String) {
        
        self.init()
        
        setImage(imgName)
        
    }
    
    func setImage(imgName : String) {
        
        setImage(UIImage(named: imgName), forState: UIControlState.Normal)
        
        setImage(UIImage(named: imgName + "_highlighted"), forState: UIControlState.Highlighted)
        
    }


}

