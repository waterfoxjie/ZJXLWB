//
//  XLStatuses.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/4.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit
import SDWebImage

class XLStatuses: NSObject {
    
    // 设置属性
    /// 微博创建时间
    var created_at : String?
    
    /// 微博id
    var id : Int = 0
    
    /// 微博信息内容
    var text : String?
    
    /// 微博来源
    var source  : String? {
    
        didSet {
        
            // 赋值
            source = source?.hrefLink().text
        }
    }
    
    /// 微博配图数组
    var pic_urls : [[String : AnyObject]]? {
    
        didSet {
        
            // 判断配图数组中是否有数据
            if pic_urls?.count == 0 {
            
                return
            }
            
            // 实例化数组
            storedImgUrls = [NSURL]()
            
            storedLargeImgUrls = [NSURL]()
            
            // 遍历字典，生成URL数组
            for dict in pic_urls! {
            
                if let urlString = dict["thumbnail_pic"] as? String {
                
                    // 生成缩略图的url
                    storedImgUrls?.append(NSURL(string: urlString)!)
                    
                    // 生成大图的url
                    let largeUrlString = urlString.stringByReplacingOccurrencesOfString("thumbnail", withString: "large")
                    
                    storedLargeImgUrls?.append(NSURL(string: largeUrlString)!)
                }
            }
        }
    }
    
    // 创建一个保存配图的数组
    private var storedImgUrls : [NSURL]?
    
    // 创建一个保存“大图”配图的数组
    private var storedLargeImgUrls : [NSURL]?
    
    // 创建一个"计算型"URL数组
    // 如果是原创的，就返回storedImgUrls，如果是转发的，就返回retweeted_status中的storedImgUrls
    // 这是因为每一条微博只有一个配图，转发的不能再进行配图，只能转发原作者的配图
    // retweeted_status == nil 表示是原创微博
    var imgUrl : [NSURL]? {
    
        return retweeted_status == nil ? storedImgUrls : retweeted_status?.storedImgUrls
    }
    
    // 定义一个大图的“计算型”URL数组
    var largeImgUrl : [NSURL]? {
    
        return retweeted_status == nil ? storedLargeImgUrls : retweeted_status?.storedLargeImgUrls
    }
    
    /// 用户
    var user : User?
    
    /// 自定义行高 
    var rowHeight : CGFloat?
    
    /// 转发微博属性
    var retweeted_status : XLStatuses?
    
   
    // MARK: - 加载微博数据，返回微博数据
    class func loadStatus(since_id : Int , max_id : Int , finished : (statusList : [XLStatuses]?, error : NSError?) -> ()) {
    
        // 数据库中加载数据（有缓存则加载，没有则从网络上加载数据）
        StatusDAL.loadStatus(since_id, max_id: max_id) { (array, error) -> () in
            
            if error != nil {
            
                finished(statusList: nil, error: error)
                
                return
            }
            
            // 获取到数组，判断能否获得数组
            if let statuses = array {
                
                // 遍历数组，字典转模型
                // 实例化一个微博数组
                var list = [XLStatuses]()
                
                for dict in statuses {
                
                    list.append(XLStatuses(dict: dict))
                    
                }
                
                // 缓存图片
                cacheWebImage(list, finished: finished)
                
            } else {
            
                finished(statusList: nil, error: nil)
            }
            
        }
        
    }

    // MARK: - 缓存图片
    private class func cacheWebImage(list : [XLStatuses] , finished : (statusList : [XLStatuses]?, error : NSError?) -> ()) {
        
        // 创建一个调度组
        let group = dispatch_group_create()
        
        // 创建变量，用于记载缓存图片的大小
        var dataLength = 0
    
        // 循环遍历数组
        for status in list {
            
            // 判断status中是否有图片
            guard let urls = status.imgUrl else {
            
                // 这里表示urls为空的情况，跳出本次循环，进入下一次
                continue
                
            }
            
            // 遍历urls，缓存图片
            for imgURL in urls {
                
                // 入组
                dispatch_group_enter(group)
                
                // 利用SDWebImage中的函数缓存图片(单例)
                SDWebImageManager.sharedManager().downloadImageWithURL(imgURL, options: SDWebImageOptions(rawValue: 0), progress: nil, completed: { (image, _, _, _, _) in
                    
                    if image != nil {
                        
                        // 将图像转换成二进制数据
                        let data = UIImagePNGRepresentation(image)!
                        
                        dataLength += data.length
                        
                    }
                    
                    // 离组
                    dispatch_group_leave(group)
                    
                })
            }

        }
        
        // 监听所有缓存操作的通知
        dispatch_group_notify(group, dispatch_get_main_queue()) { () -> Void in
            
            print("over + \(dataLength / 1024)k")
            
            // 获取完整地微博数组，可以回调
            finished(statusList: list, error: nil)
        }
    }
    
    
    // MARK: - 构造函数，从服务器返回一个字典
    init(dict : [String : AnyObject]) {
        
        super.init()
    
        // 字典转模型
        setValuesForKeysWithDictionary(dict)
        
    }
    
    // 单独设置用户属性
    override func setValue(value: AnyObject?, forKey key: String) {
        
        // 判断key是否是"user"
        if key == "user" {
        
            // 判断value是否是一个有效的字典
            if let dict = value as? [String : AnyObject] {
            
                // 创建用户数据
                user = User(dict: dict)
                
                return
            }
            
        }
        
        // 判断key是不是retweeted_status且是否有空
        if key == "retweeted_status" {
        
            if let dict = value as? [String : AnyObject] {
            
                // 创建转发微博数据
                retweeted_status = XLStatuses(dict: dict)
                
                return
            }
        }
        
        // 初始化父类中的其他属性的值（切记：一定要做这一步）
        super.setValue(value, forKey: key)
        
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
        
    }
    
    override var description : String {
        
        let keys = ["created_at" , "id" , "text" , "source" , "pic_urls"]
        
        return "\(dictionaryWithValuesForKeys(keys))"

    }
    

}
