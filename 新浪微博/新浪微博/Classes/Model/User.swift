//
//  User.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/8/4.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

class User: NSObject {
    
    // 设置属性
    /// 用户UID
    var id : Int = 0
    
    /// 友好显示名称
    var name : String?
    
    /// 用户头像地址
    var profile_image_url : String? {
        
        didSet {
        
            imgUrl = NSURL(string: profile_image_url!)
        }
    }
    
    // 头像URL
    var imgUrl : NSURL?
    
    /// 用户认证类型（-1：没有认证；0：认证用户；2、3、5：企业认证；220：草根达人）
    var verified_type  : Int = -1
    
    // 认证图标
    var imgVer : UIImage? {
    
        switch verified_type {
            
        case 0 : return UIImage(named: "avatar_vip")
            
        case 2,3,5 : return UIImage(named: "avatar_enterprise_vip")
            
        case 220 :return UIImage(named: "avatar_grassroot")
            
        default : return nil
        
        }
    }
    
    /// 用户会员等级
    var mbrank : Int = 0
    
    // 会员图标
    var imgMbrank : UIImage? {
    
        if mbrank > 0 && mbrank < 7 {
        
            return UIImage(named: "common_icon_membership_level\(mbrank)")
        }
        else {
        
            return nil
        }
    }
    
    // MARK: - 构造函数，从服务器返回一个字典
    init(dict : [String : AnyObject]) {
    
        super.init()
        
        setValuesForKeysWithDictionary(dict)
        
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
        
    }
    

}
