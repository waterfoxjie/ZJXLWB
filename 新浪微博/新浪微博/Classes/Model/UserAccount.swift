//
//  UserAccount.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/7/31.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

class UserAccount: NSObject , NSCoding {
    
    // MARK: - 设置一个类属性，判断用户是否登录
    class var userLogin : Bool {
        
        return loadAccount() != nil
        
    }
    
    
    // MARK: - 设置属性
    /// 用于调用access_token，接口获取授权后的access token
    var access_token : String?
    
    /// access_token的生命周期，单位是秒数 - 准确的数据类型是`数值`
    // 不能定义为String ： var expires_in : String?
    var expires_in : NSTimeInterval = 0 {
    
        // 也可以通过写didSet方法设置
        didSet {
        
            expiredTime = NSDate(timeIntervalSinceNow: expires_in)
        }
    }
    
    // 过期时间
    var expiredTime : NSDate?
    
    /// 当前授权用户的UID
    var uid : String?
    
    /// 用户头像地址（大图），180×180像素
    var avatar_large : String?
    
    /// 友好显示名称
    var name : String?

    
    // MARK: - 初始化字典
    init(dict : [String : AnyObject]) {
        
        super.init()
        
        // kvc
        setValuesForKeysWithDictionary(dict)
        
        // 设置全局的用户信息
        UserAccount.userAccount = self
        
    }
    
    
    // MARK: - 对象描述信息
    override var description: String {
    
        let properties = ["access_token","expires_in","uid","expiredTime","name","avatar_large"]
        
        return  "\(dictionaryWithValuesForKeys(properties))"
        
    }
    
    
    // MARK: - 归档：对象 -> 二进制
    func encodeWithCoder(aCoder: NSCoder) {
        
        aCoder.encodeObject(access_token, forKey: "access_token")
        
        aCoder.encodeDouble(expires_in, forKey: "expires_in")
        
        aCoder.encodeObject(expiredTime, forKey: "expiredTime")
        
        aCoder.encodeObject(uid, forKey: "uid")
        
        aCoder.encodeObject(name, forKey: "name")
        
        aCoder.encodeObject(avatar_large, forKey: "avatar_large")
        
    }
    
    
    // MARK: - 解档：二进制 -> 对象
    required init?(coder aDecoder: NSCoder) {
        
        access_token = aDecoder.decodeObjectForKey("access_token") as? String
        
        expires_in = aDecoder.decodeDoubleForKey("expires_in")
        
        expiredTime = aDecoder.decodeObjectForKey("expiredTime") as? NSDate
        
        uid = aDecoder.decodeObjectForKey("uid") as? String
        
        name =  aDecoder.decodeObjectForKey("name") as? String
        
        avatar_large = aDecoder.decodeObjectForKey("avatar_large") as? String
    }
    
    
    // MARK: - 归档和解档的方法
    // NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)返回的是一个数组
    // stringByAppendingPathComponent拼接路径
    static private let accountPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).last!.stringByAppendingPathComponent("account.plist")
    
    
    
    // MARK: - 保存用户账号
    func saveAccount() {
    
        // self当前对象
        // 类成员不能存储对象，所以要取需要在前面加一个类
        NSKeyedArchiver.archiveRootObject(self, toFile: UserAccount.accountPath)
        
        print(UserAccount.accountPath)
    }
    
    
    // MARK: - 加载账号
    // UserAccount?  第一次运行时、token过期时，UserAccount都访问不到
    // 静态用户账户属性
    private static var userAccount : UserAccount?
    
    class func loadAccount() -> UserAccount? {
    
        // 判断静态账户是否存在，不存在才需要解档
        // 代码优化
        if userAccount == nil {
        
            // 解档
            userAccount = NSKeyedUnarchiver.unarchiveObjectWithFile(accountPath) as? UserAccount
            
        }
        
        // 判断日期
        // 测试过期日期： userAccount!.expiredTime = NSDate(timeIntervalSinceNow: -100)
        if let date = userAccount?.expiredTime where date.compare(NSDate()) == NSComparisonResult.OrderedAscending {
        
            // 账号已过期，则清空
            userAccount = nil
        }
        
        return userAccount
        
    }
    
    
    // MARK: - 加载用户信息
    // 职责：调用方法，异步获取用户附加信息（新添加的两个属性），同时保存当前用户信息
    // 添加一个回调参数，如果有错误，返回一个错误信息
    func loadUserInfo(finished : (error : NSError?) -> ()) {
    
        // 调用网络中的加载用户函数
        NetworkTools.shareNetworkTools.loadUsersInfo(uid!) { (result, error) -> () in

            if error != nil {
            
                finished(error: error)   // 传递错误消息
                
                return
            }
            
            // 设置用户信息 对result进行强行解包
            self.name = result!["name"] as? String
            
            self.avatar_large = result!["avatar_large"] as? String
            
            // 保存用户信息
            self.saveAccount()
            
            // 完成回调
            finished(error: nil)
        }
        
        
    }
    
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
    }

}

