//
//  AppDelegate.swift
//  新浪微博
//
//  Created by waterfoxjie on 15/7/29.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

// 创建一个全局能够访问的常量,通知
let ZJRootViewControllerSwitchNotification = "ZJRootViewControllerSwitchNotification"

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // 给window属性赋值
        window = UIWindow(frame: UIScreen.mainScreen().bounds)

        // MARK: - 测试
        SQLiteManager.sharedManager
        
        // 注册通知
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "switchViewContr:", name: ZJRootViewControllerSwitchNotification, object: nil)
        
        print(UserAccount.loadAccount())
        
        // 设置颜色
        window?.backgroundColor = UIColor.whiteColor()
        
        setupAppearance()
        
        // 设置根控制器
        window?.rootViewController = defaultViewController()
        
        // 设置为主控制器
        window?.makeKeyAndVisible()
        
        return true
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        
        // 清除数据库缓存
        StatusDAL.clearDatabaseCache()
    }
    
    // 启动默认函数
    private func defaultViewController() ->  UIViewController {
    
        // 判断用户是否登录
        if !UserAccount.userLogin {
        
            return MainViewContr()
        }
        
        // 判断是否有新版本
        // 有则进去新特性界面，没有则进入欢迎界面
        return isNewUpdate() ? NewFeatureCollCon() : WelcomeViewContr()
        
    }
    
    
    // 实现通知方法
    func switchViewContr(notification : NSNotification) {
    
        print("切换控制器")
        
        let mainVc = notification.object as! Bool
        
        window?.rootViewController = mainVc ? MainViewContr() : WelcomeViewContr()
        
    }
    
    // 注销通知：当程序被注销时，才会执行的一个函数，不写也是可以的
    deinit {
    
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    // 检查新版本
    private func isNewUpdate() -> Bool {
        
        // 1、获取当前的版本
        let nowEdition = Double(NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as! String)!
        
        // 2、获取之前的版本（从偏好设置中获取）
        let sandboxVersionKey = "sandboxVersionKey"
        
        // 从偏好设置中获取对应key的值
        let sandboxVersion = NSUserDefaults.standardUserDefaults().doubleForKey(sandboxVersionKey)
        
        // 3、将当前的版本存储到偏好设置中
        NSUserDefaults.standardUserDefaults().setDouble(nowEdition, forKey: sandboxVersionKey)
        
        // 4、返回比较结果
        return nowEdition > sandboxVersion
    }
    
    // 设置外观
    private func setupAppearance() {
    
        // 设置状态栏外观
        UINavigationBar.appearance().tintColor = UIColor.orangeColor()
        
        // 设置TabBar外观
        UITabBar.appearance().tintColor = UIColor.orangeColor()
        
    }
    

}

